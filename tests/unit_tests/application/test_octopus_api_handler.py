from datetime import datetime, timedelta
from unittest.mock import MagicMock, call

import pytest

from octopus_energy_usage_reporter.application import OctopusAPIHandler


@pytest.fixture
def mock_handler():
    class MockHandler(OctopusAPIHandler):
        def __init__(self):
            super().__init__()

            self.logger = MagicMock()

    yield MockHandler()


class TestExecuteOctopusAPIHandler:
    @pytest.fixture
    def params(self):
        class Params:
            api_auth = "MyAPIAuth"
            product_name = "MyProductName"
            tariff_version = "MyTariffVersion"
            product_start_date = datetime(2022, 6, 25, 0, 0, 0).isoformat()
            electricity_register = "MyElectricityRegister"
            mban = "MyMBAN"
            serial_number = "MySerialNumber"
            night_start_date = datetime(2022, 6, 25, 0, 0, 0)
            night_end_date = datetime(2022, 6, 25, 6, 59, 59)
            day_start_date = datetime(2022, 6, 25, 7, 0, 0)
            day_end_date = datetime(2022, 6, 25, 23, 59, 59)

        yield Params()

    @pytest.fixture
    def get_params_response(self, params):
        yield {
            "/octopus-energy-usage-reporter/APIAuth": params.api_auth,
            "/octopus-energy-usage-reporter/ProductName": params.product_name,
            "/octopus-energy-usage-reporter/TariffVersion": params.tariff_version,
            "/octopus-energy-usage-reporter/ProductStartDate": params.product_start_date,
            "/octopus-energy-usage-reporter/ElectricityRegister": params.electricity_register,
            "/octopus-energy-usage-reporter/MBAN": params.mban,
            "/octopus-energy-usage-reporter/SerialNumber": params.serial_number,
        }

    @pytest.fixture
    def tariff_prices_response(self, tmp_path):
        def _response(**kwargs):
            standard_unit_rates = kwargs.get("standard_unit_rates", "N/A")
            day_unit_rates = kwargs.get("day_unit_rates", "N/A")
            night_unit_rates = kwargs.get("night_unit_rates", "N/A")
            standing_charges = kwargs.get("standing_charges", 40.259)

            return {
                "standard-unit-rates": [{"value_inc_vat": standard_unit_rates}]
                if standard_unit_rates != "N/A"
                else standard_unit_rates,
                "day-unit-rates": [{"value_inc_vat": day_unit_rates}]
                if day_unit_rates != "N/A"
                else day_unit_rates,
                "night-unit-rates": [{"value_inc_vat": night_unit_rates}]
                if night_unit_rates != "N/A"
                else night_unit_rates,
                "standing-charges": [{"value_inc_vat": standing_charges}]
                if standing_charges != "N/A"
                else standing_charges,
            }

        return _response

    @pytest.fixture
    def get_time_ranges_response(self, params):
        yield [
            (
                params.night_start_date,
                params.night_end_date,
            ),
            (
                params.day_start_date,
                params.day_end_date,
            ),
        ]

    def test_execute_calls_put_item_with_standard_rate(
        self,
        tmp_path,
        mock_handler,
        params,
        get_params_response,
        tariff_prices_response,
        get_time_ranges_response,
    ):

        tariff_result = tariff_prices_response(standard_unit_rates=19.765)

        start_date = datetime(2022, 6, 25, 0, 0, 0)
        end_date = datetime(2022, 6, 25, 23, 59, 59)

        mock_handler.start_date = start_date
        mock_handler.end_date = end_date
        expiration = (start_date + timedelta(days=365 * 2)).timestamp()

        mock_handler.get_params = MagicMock(return_value=get_params_response)

        mock_handler.get_tariff_prices = MagicMock(return_value=tariff_result)

        mock_handler.get_time_ranges = MagicMock(side_effect=get_time_ranges_response)

        mock_handler.get_meter_consumption = MagicMock(side_effect=[4.25, 11.25])

        mock_handler.put_item = MagicMock()

        expected_args = {
            "table_name": "my_table",
            "payload": {
                "Month": "June",
                "DateEpoch": start_date.timestamp(),
                "NightkWh": 4.25,
                "DaykWh": 11.25,
                "DayRate": 19.765,
                "NightRate": 19.765,
                "StandingCharge": 40.259,
                "NightStartHour": "00:00",
                "NightEndHour": "07:00",
                "WeekDay": "Saturday",
                "Expire": expiration,
            },
        }

        mock_handler.execute()

        mock_handler.get_params.assert_called_once_with(list(get_params_response.keys()))
        mock_handler.get_tariff_prices.assert_called_once_with(
            params.api_auth,
            params.product_name,
            params.tariff_version,
            params.product_start_date,
            params.electricity_register,
            start_date.isoformat(),
            end_date.isoformat(),
        )
        mock_handler.get_time_ranges.assert_has_calls(
            [
                call(start_date, "00:00:00", "06:59:59"),
                call(start_date, "07:00:00", "23:59:59"),
            ]
        )
        mock_handler.get_meter_consumption.assert_has_calls(
            [
                call(
                    params.api_auth,
                    params.mban,
                    params.serial_number,
                    params.night_start_date.isoformat(),
                    params.night_end_date.isoformat(),
                ),
                call(
                    params.api_auth,
                    params.mban,
                    params.serial_number,
                    params.day_start_date.isoformat(),
                    params.day_end_date.isoformat(),
                ),
            ]
        )
        mock_handler.put_item.assert_called_once_with(*expected_args.values())

        mock_handler.logger.info.assert_called_once_with("Tariff details", tariff_result)

    def test_execute_calls_put_item_with_day_night_rate(
        self,
        tmp_path,
        mock_handler,
        params,
        get_params_response,
        tariff_prices_response,
        get_time_ranges_response,
    ):

        tariff_result = tariff_prices_response(
            day_unit_rates=19.765, night_unit_rates=11.765
        )

        start_date = datetime(2022, 6, 25, 0, 0, 0)
        end_date = datetime(2022, 6, 25, 23, 59, 59)
        expiration = (start_date + timedelta(days=365 * 2)).timestamp()

        mock_handler.start_date = start_date
        mock_handler.end_date = end_date

        mock_handler.get_params = MagicMock(return_value=get_params_response)

        mock_handler.get_tariff_prices = MagicMock(return_value=tariff_result)

        mock_handler.get_time_ranges = MagicMock(side_effect=get_time_ranges_response)

        mock_handler.get_meter_consumption = MagicMock(side_effect=[4.25, 11.25])

        mock_handler.put_item = MagicMock()

        expected_args = {
            "table_name": "my_table",
            "payload": {
                "Month": "June",
                "DateEpoch": start_date.timestamp(),
                "NightkWh": 4.25,
                "DaykWh": 11.25,
                "DayRate": 19.765,
                "NightRate": 11.765,
                "StandingCharge": 40.259,
                "NightStartHour": "00:00",
                "NightEndHour": "07:00",
                "WeekDay": "Saturday",
                "Expire": expiration,
            },
        }

        mock_handler.execute()

        mock_handler.get_params.assert_called_once_with(list(get_params_response.keys()))
        mock_handler.get_tariff_prices.assert_called_once_with(
            params.api_auth,
            params.product_name,
            params.tariff_version,
            params.product_start_date,
            params.electricity_register,
            start_date.isoformat(),
            end_date.isoformat(),
        )
        mock_handler.get_time_ranges.assert_has_calls(
            [
                call(start_date, "00:00:00", "06:59:59"),
                call(start_date, "07:00:00", "23:59:59"),
            ]
        )
        mock_handler.get_meter_consumption.assert_has_calls(
            [
                call(
                    params.api_auth,
                    params.mban,
                    params.serial_number,
                    params.night_start_date.isoformat(),
                    params.night_end_date.isoformat(),
                ),
                call(
                    params.api_auth,
                    params.mban,
                    params.serial_number,
                    params.day_start_date.isoformat(),
                    params.day_end_date.isoformat(),
                ),
            ]
        )
        mock_handler.put_item.assert_called_once_with(*expected_args.values())

        mock_handler.logger.info.assert_called_once_with("Tariff details", tariff_result)
