from datetime import datetime
from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.application import ParseDatesHandler
from tests.unit_tests.factories import create_parse_dates_event


class TestParseDatesHandler:
    @pytest.fixture()
    def handler(self):
        class MockHandler(ParseDatesHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockHandler()

    def test_parse_event_parses_iso_strings(self, handler):
        handler.event = create_parse_dates_event()

        handler.parse_event()

        assert handler.start_date == datetime(2023, 4, 1, 0, 0, 0)
        assert handler.end_date == datetime(2023, 4, 30, 0, 0, 0)

    def test_format_date_creates_dynamodb_object(self, handler):
        date = datetime(2023, 4, 18, 0, 0, 0)

        expected_format = {
            "Month": "April",
            "Date": str(date.timestamp()),
            "Day": "Tuesday",
        }

        assert handler.format_date(date) == expected_format

    def test_execute_returns_list_of_date_formats(self, handler):
        event = create_parse_dates_event(
            start_date="2023-04-01T00:00:00Z",
            end_date="2023-04-03T00:00:00Z",
        )

        expected_result = [
            {
                "Month": "April",
                "Date": "1680303600.0",
                "Day": "Saturday",
            },
            {
                "Month": "April",
                "Date": "1680390000.0",
                "Day": "Sunday",
            },
            {
                "Month": "April",
                "Date": "1680476400.0",
                "Day": "Monday",
            },
        ]

        handler.event = event
        handler.parse_event()

        assert handler.execute() == expected_result
