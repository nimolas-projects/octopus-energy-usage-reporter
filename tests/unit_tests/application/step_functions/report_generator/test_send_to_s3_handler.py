from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.application import SendToS3Handler
from tests.unit_tests.factories import create_send_to_s3_handler_event


class TestSendToS3Handler:
    @pytest.fixture()
    def handler(self):
        class MockHandler(SendToS3Handler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockHandler()

    def test_parse_event_parses_file_details(self, handler):
        event = create_send_to_s3_handler_event()

        handler.event = event
        handler.parse_event()

        assert handler.file_name == "Super/Cool/S3/Dir"
        assert handler.file_data == b"A cool byte string"

    def test_execute_calls_put_object_and_returns_object_prefix(self, handler):
        event = create_send_to_s3_handler_event()

        expected_parameters = {
            "Bucket": "my_bucket",
            "Key": "Super/Cool/S3/Dir",
            "Body": b"A cool byte string",
        }
        handler.put_object = MagicMock()

        handler.event = event
        handler.parse_event()

        result = handler.execute()

        handler.put_object.assert_called_once_with(expected_parameters)
        assert result == "Super/Cool"
