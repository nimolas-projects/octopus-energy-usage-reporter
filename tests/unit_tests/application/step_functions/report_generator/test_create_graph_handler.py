from io import BytesIO
from unittest.mock import MagicMock, patch

import pytest

from octopus_energy_usage_reporter.application import CreateGraphHandler
from tests.unit_tests.factories import create_graph_handler_event


class TestCreateGraphHandler:
    @pytest.fixture()
    def handler(self):
        class MockHandler(CreateGraphHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

                self.create_pie_graph = MagicMock(
                    return_value=BytesIO(b"A cool byte string")
                )
                self.graph_type_functions["pie_graph"] = self.create_pie_graph

                self.create_bar_graph = MagicMock(
                    return_value=BytesIO(b"A cool byte string")
                )
                self.graph_type_functions["bar_graph"] = self.create_bar_graph

        yield MockHandler()

    @pytest.fixture()
    def encoded_binary_data(self):
        yield "eJxzVEjOz89RSKosSVUoLinKzEsHADrLBpo="

    def test_execute_returns_graph_data_for_pie_chart(
        self, handler, encoded_binary_data
    ):
        expected_response = {
            "file_name": "Super/Cool/S3/Dir.png",
            "file_data": encoded_binary_data,
        }

        event = create_graph_handler_event()

        handler.event = event
        handler.parse_event()

        assert handler.execute() == expected_response
        handler.create_pie_graph.assert_called_once_with(
            event["body"]["s3_file_name"], **event["body"]["graph_data"]
        )

    def test_execute_returns_graph_data_for_bar_chart(
        self, handler, encoded_binary_data
    ):
        expected_response = {
            "file_name": "Super/Cool/S3/Dir.png",
            "file_data": encoded_binary_data,
        }

        handler.event = create_graph_handler_event(type="bar_graph")
        handler.parse_event()

        with patch(
            (
                "octopus_energy_usage_reporter.application."
                "step_functions.report_generator."
                "create_graphs_handler.BytesIO"
            )
        ) as BytesIO:
            BytesIO.getvalue = MagicMock(return_value=b"A cool byte string")

            assert handler.execute() == expected_response
