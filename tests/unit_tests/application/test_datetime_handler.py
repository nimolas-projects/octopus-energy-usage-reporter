import os
from datetime import datetime
from unittest.mock import MagicMock, call

import pytest

from octopus_energy_usage_reporter.application.datetime_handler import DateTimeHandler
from tests.unit_tests.factories import cron_event_response

SQS_QUEUE = os.environ.get("SQS_QUEUE")


class TestDateTimeHandler:
    @pytest.fixture()
    def logger(self):
        logger = MagicMock()

        yield logger

    @pytest.fixture()
    def handler(self, logger):
        class MockedHandler(DateTimeHandler):
            def __init__(self) -> None:
                super().__init__()

                self.logger = logger

        yield MockedHandler()

    def test_parse_event_pulls_out_timestamp_from_cron_jon(self, handler):
        event = cron_event_response()

        handler.event = event
        handler.parse_event()

        assert handler.cron_time == datetime(2022, 4, 15, 21, 45)

    def test_execute_calls_send_message_to_sqs(self, handler):
        handler.parse_timestamp = MagicMock()
        handler.subtract_days_from_date = MagicMock(
            return_value=datetime(2022, 4, 17, 0, 0, 0)
        )
        handler.send_message_to_sqs = MagicMock()

        handler.correlation_id = "Super-Sick-UUID"
        handler.cron_time = "2022-04-18T00:00:00Z"

        expected_sqs_payload = {
            "queue_url": SQS_QUEUE,
            "message_body": {
                "start_date": "2022-04-17T00:00:00Z",
                "end_date": "2022-04-17T23:59:59Z",
            },
            "message_attributes": {"correlation_id": "Super-Sick-UUID"},
        }

        expected_info_calls = [
            call("Time from the cron job", {"time": "2022-04-18T00:00:00Z"}),
        ]

        handler.execute()

        handler.logger.info.has_calls(expected_info_calls)

        handler.send_message_to_sqs.assert_called_with(**expected_sqs_payload)
