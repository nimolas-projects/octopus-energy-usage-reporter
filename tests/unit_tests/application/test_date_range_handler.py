import json
from datetime import datetime
from unittest.mock import MagicMock, call

import pytest

from octopus_energy_usage_reporter.application import DateRangeHandler
from tests.unit_tests.factories import api_event


class TestDateRangeHandler:
    @pytest.fixture()
    def handler(self):
        class MockHandler(DateRangeHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockHandler()

    def test_parse_event_gets_dates_from_body(self, handler):
        handler.event = api_event()

        handler.parse_event()

        assert handler.start_date == datetime(2022, 6, 1, 0, 0, 0)
        assert handler.end_date == datetime(2022, 6, 2, 0, 0, 0)

    def test_execute_sends_to_sqs_and_returns_response_for_single_date(self, handler):
        handler.event = api_event()
        handler.event["body"] = json.dumps(
            ["2022-06-01T00:00:00Z", "2022-06-01T00:00:00Z"]
        )
        handler.correlation_id = "Super-Sick-UUID"
        handler.send_message_to_sqs = MagicMock()

        handler.parse_event()

        expected_sqs_calls = [
            call(
                **{
                    "queue_url": "https://queue-url.com",
                    "message_body": {
                        "start_date": "2022-06-01T00:00:00Z",
                        "end_date": "2022-06-01T23:59:59Z",
                    },
                    "message_attributes": {"correlation_id": "Super-Sick-UUID"},
                }
            ),
        ]

        expected_response = {
            "message": "Sent 1 dates to SQS Queue for processing",
            "dates": ["2022-06-01T00:00:00"],
        }

        assert handler.execute() == expected_response
        handler.send_message_to_sqs.assert_has_calls(expected_sqs_calls)

        handler.logger.info.assert_called_once_with(
            "Date range from the api event",
            {
                "start_date": datetime(2022, 6, 1, 0, 0, 0),
                "end_date": datetime(2022, 6, 1, 0, 0, 0),
            },
        )

    def test_execute_sends_to_sqs_and_returns_response_for_multiple_dates(self, handler):
        handler.event = api_event()
        handler.correlation_id = "Super-Sick-UUID"
        handler.send_message_to_sqs = MagicMock()

        handler.parse_event()

        expected_sqs_calls = [
            call(
                **{
                    "queue_url": "https://queue-url.com",
                    "message_body": {
                        "start_date": "2022-06-01T00:00:00Z",
                        "end_date": "2022-06-01T23:59:59Z",
                    },
                    "message_attributes": {"correlation_id": "Super-Sick-UUID"},
                }
            ),
            call(
                **{
                    "queue_url": "https://queue-url.com",
                    "message_body": {
                        "start_date": "2022-06-02T00:00:00Z",
                        "end_date": "2022-06-02T23:59:59Z",
                    },
                    "message_attributes": {"correlation_id": "Super-Sick-UUID"},
                }
            ),
        ]

        expected_response = {
            "message": "Sent 2 dates to SQS Queue for processing",
            "dates": ["2022-06-01T00:00:00", "2022-06-02T00:00:00"],
        }

        assert handler.execute() == expected_response
        handler.send_message_to_sqs.assert_has_calls(expected_sqs_calls)

        handler.logger.info.assert_called_once_with(
            "Date range from the api event",
            {
                "start_date": datetime(2022, 6, 1, 0, 0, 0),
                "end_date": datetime(2022, 6, 2, 0, 0, 0),
            },
        )
