def sqs_send_message_response():
    return {
        "MD5OfMessageBody": "string",
        "MD5OfMessageAttributes": "string",
        "MD5OfMessageSystemAttributes": "string",
        "MessageId": "string",
        "SequenceNumber": "string",
    }
