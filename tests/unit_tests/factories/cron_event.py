def cron_event_response():
    return {
        "version": "0",
        "id": "7cf9d17f-3626-440b-8aae-b0ee430f681c",
        "detail-type": "Scheduled Event",
        "source": "aws.events",
        "account": "1234567",
        "time": "2022-04-15T21:45:00Z",
        "region": "eu-west-2",
        "resources": ["arn:aws:events:eu-west-2: 1234567:rule/DateTimeSchedule"],
        "detail": {},
    }
