from datetime import datetime


def get_param_response(**kwargs):
    param_name = kwargs.get("param_name", "MyParam")
    type = kwargs.get("type", "String")
    value = kwargs.get("value", "MyValue")

    return {
        "Parameter": {
            "Name": param_name,
            "Type": type,
            "Value": value,
            "Version": 123,
            "Selector": "string",
            "SourceResult": "string",
            "LastModifiedDate": datetime(2015, 1, 1),
            "ARN": "string",
            "DataType": "string",
        }
    }


def get_params_response(**kwargs):
    params = kwargs.get("params", {})
    invalid_params = kwargs.get("invalid_params", [])

    response = {
        "Parameters": [
            {
                "Name": param["Name"],
                "Type": param["Type"],
                "Value": param["Value"],
                "Version": 123,
                "Selector": "string",
                "SourceResult": "string",
                "LastModifiedDate": datetime(2015, 1, 1),
                "ARN": "string",
                "DataType": "string",
            }
            for param in params
        ],
    }

    if invalid_params:
        response["InvalidParameters"] = invalid_params

    return response
