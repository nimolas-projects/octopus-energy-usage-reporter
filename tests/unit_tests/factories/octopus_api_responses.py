def list_products_response():
    return {
        "count": 101,
        "next": None,
        "previous": None,
        "results": [
            {
                "code": "1201",
                "direction": "IMPORT",
                "full_name": "MyProductName",
                "display_name": "MyProductName",
                "description": "Affect Standard Tariff",
                "is_variable": True,
                "is_green": False,
                "is_tracker": False,
                "is_prepay": False,
                "is_business": False,
                "is_restricted": False,
                "term": None,
                "available_from": "2016-01-01T00:00:00Z",
                "available_to": "2020-09-30T15:00:00+01:00",
                "links": [
                    {
                        "href": "https://api.octopus.energy/v1/products/1201/",
                        "method": "GET",
                        "rel": "self",
                    }
                ],
                "brand": "AFFECT_ENERGY",
            }
        ],
    }


def retrieve_product_response():
    return {
        "code": "1201",
        "full_name": "MyProductName",
        "display_name": "Octopus 12M Fixed",
        "description": (
            "This tariff features 100% renewable electricity and "
            "fixes your unit rates and standing charge for 12 months. "
            "There are no exit fees, so if you change your mind,"
            " you're in control."
        ),
        "is_variable": False,
        "is_green": False,
        "is_tracker": False,
        "is_prepay": False,
        "is_business": False,
        "is_restricted": False,
        "term": 12,
        "available_from": "2020-02-12T00:00:00Z",
        "available_to": "2020-09-16T00:00:00+01:00",
        "tariffs_active_at": "2022-06-19T19:38:21.760650Z",
        "single_register_electricity_tariffs": {
            "_A": {
                "direct_debit_monthly": {
                    "code": "MyTariffCode-A",
                    "standing_charge_exc_vat": 20.64,
                    "standing_charge_inc_vat": 21.672,
                    "online_discount_exc_vat": 0,
                    "online_discount_inc_vat": 0,
                    "dual_fuel_discount_exc_vat": 0,
                    "dual_fuel_discount_inc_vat": 0,
                    "exit_fees_exc_vat": 0,
                    "exit_fees_inc_vat": 0,
                    "exit_fees_type": "NONE",
                    "links": [
                        {
                            "href": "https://api.octopus.energy/v1/products/FIX-12M-20-02-12/electricity-tariffs/E-1R-FIX-12M-20-02-12-A/standing-charges/",
                            "method": "GET",
                            "rel": "standing_charges",
                        },
                        {
                            "href": "https://api.octopus.energy/v1/products/FIX-12M-20-02-12/electricity-tariffs/E-1R-FIX-12M-20-02-12-A/standard-unit-rates/",
                            "method": "GET",
                            "rel": "standard_unit_rates",
                        },
                    ],
                    "standard_unit_rate_exc_vat": 14.31,
                    "standard_unit_rate_inc_vat": 15.0255,
                }
            }
        },
    }


def successful_tariff_charges_response():
    return {
        "count": 1,
        "next": None,
        "previous": None,
        "results": [
            {
                "value_exc_vat": 18.8,
                "value_inc_vat": 19.74,
                "valid_from": "2021-10-13T23:00:00Z",
                "valid_to": "2022-04-01T23:00:00Z",
            }
        ],
    }


def failed_tariff_charges_response():
    return {"detail": "This tariff has standard rates, not day and night."}


def get_consumption_response():
    return {
        "count": 1,
        "next": None,
        "previous": None,
        "results": [
            {
                "consumption": 8.705,
                "interval_start": "2022-06-05T07:00:00+01:00",
                "interval_end": "2022-06-06T00:00:00+01:00",
            }
        ],
    }
