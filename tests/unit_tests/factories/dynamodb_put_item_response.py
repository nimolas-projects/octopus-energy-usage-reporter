def put_item_response():
    return {
        "ResponseMetadata": {
            "RequestId": ("R4DDC4L829RIFVMG8NFIB2RR" "E3VV4KQNSO5AEMVJF66Q9ASUAAJG"),
            "HTTPStatusCode": 200,
            "HTTPHeaders": {
                "server": "Server",
                "date": "Sun, 19 Jun 2022 04:00:20 GMT",
                "content-type": "application/x-amz-json-1.0",
                "content-length": "2",
                "connection": "keep-alive",
                "x-amzn-requestid": (
                    "R4DDC4L829RIFVMG8NFIB2RRE" "3VV4KQNSO5AEMVJF66Q9ASUAAJG"
                ),
                "x-amz-crc32": "2745614147",
            },
            "RetryAttempts": 0,
        }
    }
