def create_step_function_event_base(body, correlation_id=None):
    if correlation_id is None:
        correlation_id = "Super-Cool-UUID"

    return {
        "body": body,
        "metadata": {"correlation_id": correlation_id},
    }
