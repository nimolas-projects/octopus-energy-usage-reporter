from .step_function_event_base import create_step_function_event_base


def _pie_chart_data():
    return {
        "data": [
            {
                "values": [
                    16.124,
                    18.848,
                    18.511,
                    15.936,
                    15.397,
                    18.189,
                    15.217,
                ],
                "title": "max",
                "legend_title": "Weekdays",
                "labels": [
                    "Sunday",
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                ],
            }
        ],
        "plot_rows": 1,
        "plot_columns": 1,
    }


def create_graph_handler_event(**event_kwargs):
    s3_file_name = event_kwargs.get("s3_file_name", "Super/Cool/S3/Dir")
    tmp_file_name = event_kwargs.get("tmp_file_name", "Super/Cool/Temp/Dir")
    graph_type = event_kwargs.get("type", "pie_graph")
    graph_data = event_kwargs.get("graph_data", {**_pie_chart_data()})

    body = {
        "s3_file_name": s3_file_name,
        "tmp_file_name": tmp_file_name,
        "type": graph_type,
        "graph_data": graph_data,
    }

    return create_step_function_event_base(body)
