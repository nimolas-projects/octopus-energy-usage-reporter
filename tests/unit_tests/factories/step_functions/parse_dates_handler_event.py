def create_parse_dates_event(**event_kwargs):
    start_date = event_kwargs.get("start_date", "2023-04-01T00:00:00Z")
    end_date = event_kwargs.get("end_date", "2023-04-30T00:00:00Z")

    return {"start_date": start_date, "end_date": end_date}
