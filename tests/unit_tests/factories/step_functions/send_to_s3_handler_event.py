from .step_function_event_base import create_step_function_event_base


def create_send_to_s3_handler_event(**event_kwargs):
    file_name = event_kwargs.get("file_name", "Super/Cool/S3/Dir")
    file_data = event_kwargs.get("file_data", "eJxzVEjOz89RSKosSVUoLinKzEsHADrLBpo=")

    body = {
        "file_name": file_name,
        "file_data": file_data,
    }

    return create_step_function_event_base(body)
