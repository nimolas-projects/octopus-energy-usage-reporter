def query_response():
    return {
        "Items": [
            {
                "NightEndHour": {"S": "07:00"},
                "Expire": {"N": "1721001600"},
                "NightkWh": {"N": "3.656"},
                "DayRate": {"N": "27.6255"},
                "NightRate": {"N": "27.6255"},
                "NightStartHour": {"S": "00:00"},
                "Month": {"S": "July"},
                "StandingCharge": {"N": "44.478"},
                "DateEpoch": {"N": "1657929600"},
                "WeekDay": {"S": "Saturday"},
                "DaykWh": {"N": "7.91"},
            }
        ],
        "Count": 1,
        "ScannedCount": 1,
        "ResponseMetadata": {
            "RequestId": ("L0E9E55NOAADEOGNAAPD64U1CFVV4" "KQNSO5AEMVJF66Q9ASUAAJG"),
            "HTTPStatusCode": 200,
            "HTTPHeaders": {
                "server": "Server",
                "date": "Sun, 24 Jul 2022 15:01:14 GMT",
                "content-type": "application/x-amz-json-1.0",
                "content-length": "1849",
                "connection": "keep-alive",
                "x-amzn-requestid": (
                    "L0E9E55NOAADEOGNAAPD64U1CFVV4" "KQNSO5AEMVJF66Q9ASUAAJG"
                ),
                "x-amz-crc32": "1155034297",
            },
            "RetryAttempts": 0,
        },
    }


def parsed_query_items():
    return [
        {
            "NightEndHour": "07:00",
            "Expire": 1721001600,
            "NightkWh": 3.656,
            "DayRate": 27.6255,
            "NightRate": 27.6255,
            "NightStartHour": "00:00",
            "Month": "July",
            "StandingCharge": 44.478,
            "DateEpoch": 1657929600,
            "WeekDay": "Saturday",
            "DaykWh": 7.91,
        },
        {
            "NightEndHour": "07:00",
            "Expire": 1721088000,
            "NightkWh": 1.440,
            "DayRate": 27.6255,
            "NightRate": 27.6255,
            "NightStartHour": "00:00",
            "Month": "July",
            "StandingCharge": 44.478,
            "DateEpoch": 1658016000,
            "WeekDay": "Sunday",
            "DaykWh": 13.101,
        },
        {
            "NightEndHour": "07:00",
            "Expire": 1721174400,
            "NightkWh": 0.120,
            "DayRate": 27.6255,
            "NightRate": 27.6255,
            "NightStartHour": "00:00",
            "Month": "July",
            "StandingCharge": 44.478,
            "DateEpoch": 1658102400,
            "WeekDay": "Monday",
            "DaykWh": 2.374,
        },
    ]


def parse_weekday_query_items():
    return [*parsed_query_items()[:2]] * 2
