def sqs_event(**kwargs):
    body = kwargs.get("body", "{}")
    message_attributes = kwargs.get("message_attributes", {})

    return {
        "Records": [
            {
                "messageId": "0a4c4f7f-b28c-4993-8036-132117204838",
                "receiptHandle": (
                    "AQEBCXxVDWlcfDRdn7jKnu3rqGY6vPgZ8IzZFPtUgInPqK"
                    "D77WIKnZNs2dyIks1LEqdm6vRWsuRvSraEkAiDJVW37QM3"
                    "wt3QdCkAF73pKhkAyJVYoYNfl4U33t6K8ao2OXkf9fpJP2"
                    "1YqpVmX1wSzztzT6QaMYwjxEGooYewj4lO7ylmcUfqCAIy"
                    "yRIfy/DSSwE1jai85gquEd0A8/hb69/f//GIXiw+kYo41P"
                    "zEQtTeMqo5+fj7XsU6znfmUpa88ZGO+U8VTA5gDwH8ry3k"
                    "stEJzi0LFtpKINee+TnLSCg4tIsZkbUlrMmp+QV/QCWxjJ"
                    "e0romNYY/eoeI0QHTf5BrdKwhDfri0HteRZNm1K+s7jL1T"
                    "BZzLoySXuG3g77oKHD7bvxBenSkheDR3A9HxoSDL1voyNr"
                    "5mw7dWDH6gkxHVXxiTo/yfLaQ/idl5p3dHLp5zVfSc6sFW"
                    "rDWFRgYwWsfQhw=="
                ),
                "body": body,
                "attributes": {
                    "ApproximateReceiveCount": "1",
                    "SentTimestamp": "1650273704318",
                    "SenderId": "676569277244",
                    "ApproximateFirstReceiveTimestamp": "1650273704323",
                },
                "messageAttributes": message_attributes,
                "md5OfBody": "3d00c72f0c99ba28d5ddd8db618888fe",
                "eventSource": "aws:sqs",
                "eventSourceARN": (
                    "arn:aws:sqs:eu-west-2:676569277244:octopus-energy"
                    "-usage-reporter-OctopusAPI-Handler-DateTimeSQS-i4"
                    "WtaWuUGWEl"
                ),
                "awsRegion": "eu-west-2",
            }
        ]
    }
