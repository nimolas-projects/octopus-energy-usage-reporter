from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.infrastructure import StepFunctionStateHandler
from tests.unit_tests.factories import create_step_function_event_base


class TestStepFunctionStateHandler:
    @pytest.fixture()
    def handler_type(self):
        class MockHandler(StepFunctionStateHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockHandler

    @pytest.fixture()
    def handler(self, handler_type):
        yield handler_type()

    def test_parse_event_separates_body_and_metadata(self, handler):
        event = create_step_function_event_base({"cool": "body"})

        handler.event = event
        handler.parse_event()

        assert handler.body == {"cool": "body"}
        assert handler.metadata == {"correlation_id": "Super-Cool-UUID"}
        assert handler.correlation_id == "Super-Cool-UUID"

    def test_handler_function_returns_correct_structure_when_body(self, handler_type):
        class MockHandler(handler_type):
            def execute(self):
                return {"cool": "body"}

        expected_response = {
            "body": {"cool": "body"},
            "metadata": {"correlation_id": "Super-Cool-UUID"},
            "status_code": 200,
        }

        handler = MockHandler()
        event = create_step_function_event_base(None)

        assert handler.handler_function(event, None) == expected_response

    def test_handler_function_returns_correct_structure_when_no_body(self, handler_type):
        class MockHandler(handler_type):
            def execute(self):
                return None

        expected_response = {
            "body": None,
            "metadata": {"correlation_id": "Super-Cool-UUID"},
            "status_code": 200,
        }

        handler = MockHandler()
        event = create_step_function_event_base(None)

        assert handler.handler_function(event, None) == expected_response
