import json
from unittest.mock import MagicMock, call

import pytest
from botocore.exceptions import ClientError
from botocore.stub import Stubber

from octopus_energy_usage_reporter.infrastructure import SQSSendMessageMixin
from tests.unit_tests.factories import sqs_send_message_response


class TestSQSSendMessageMixin:
    @pytest.fixture()
    def mixin(self):
        class MockMixin(SQSSendMessageMixin):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockMixin()

    @pytest.fixture()
    def stub(self):
        from octopus_energy_usage_reporter.infrastructure.sqs_send_message_mixin import (
            sqs_client,
        )

        with Stubber(sqs_client) as stubber:
            yield stubber

            stubber.assert_no_pending_responses()

    @pytest.fixture()
    def expected_params(self):
        yield {
            "QueueUrl": "https://some-queue-URL",
            "MessageBody": json.dumps(
                {
                    "start_date": "16/04/22 00:00:00",
                    "end_date": "16/04/22 23:59:59",
                }
            ),
            "MessageAttributes": {
                "correlation_id": {
                    "StringValue": "Super-Sick-UUID",
                    "DataType": "String",
                }
            },
        }

    @pytest.fixture()
    def function_params(self):
        yield {
            "queue_url": "https://some-queue-URL",
            "message_body": {
                "start_date": "16/04/22 00:00:00",
                "end_date": "16/04/22 23:59:59",
            },
            "message_attributes": {
                "correlation_id": "Super-Sick-UUID",
            },
        }

    def test_send_message_to_sqs_calls_send_message(
        self, stub, mixin, expected_params, function_params
    ):
        expected_response = sqs_send_message_response()

        expected_info_calls = [
            call("Sending payload to SQS", expected_params),
            call("Response from SQS", expected_response),
        ]

        stub.add_response("send_message", expected_response, expected_params)

        mixin.send_message_to_sqs(**function_params)

        mixin.logger.info.has_calls(expected_info_calls)

    def test_send_message_to_sqs_handles_number_message_attributes(
        self, stub, mixin, expected_params, function_params
    ):
        expected_response = sqs_send_message_response()

        expected_params["MessageAttributes"]["correlation_id"]["StringValue"] = "1234"
        expected_params["MessageAttributes"]["correlation_id"]["DataType"] = "Number"

        function_params["message_attributes"]["correlation_id"] = 1234

        expected_info_calls = [
            call("Sending payload to SQS", expected_params),
            call("Response from SQS", expected_response),
        ]

        stub.add_response("send_message", expected_response, expected_params)

        mixin.send_message_to_sqs(**function_params)

        mixin.logger.info.has_calls(expected_info_calls)

    def test_send_message_to_sqs_raises_on_exception(
        self, stub, mixin, expected_params, function_params
    ):
        stub.add_client_error(
            "send_message",
            service_error_code="NoSuchQueue",
            service_message="Provided queue was not found",
            expected_params=expected_params,
        )

        with pytest.raises(ClientError) as error:
            mixin.send_message_to_sqs(**function_params)

        mixin.logger.error.assert_called_once_with(
            "Error occured when sending to SQS", expected_params, error.value
        )
