from unittest.mock import MagicMock

import pytest
from botocore.exceptions import ClientError
from botocore.stub import Stubber

from octopus_energy_usage_reporter.infrastructure import SSMParamMixin
from tests.unit_tests.factories import get_param_response, get_params_response


class TestSSMParamMixin:
    @pytest.fixture()
    def handler(self):
        class MockSSMParamMixin(SSMParamMixin):
            def __init__(self) -> None:
                super().__init__()

                self.logger = MagicMock()

        yield MockSSMParamMixin()

    @pytest.fixture()
    def stub(self):
        from octopus_energy_usage_reporter.infrastructure.ssm_param_mixin import (
            ssm_client,
        )

        with Stubber(ssm_client) as stubber:
            yield stubber

            stubber.assert_no_pending_responses()

    def test_get_param_returns_param(self, handler, stub):
        param_name = "/OEUR/MyParam"

        expected_response = {"Name": "/OEUR/MyParam", "Value": "MyValue"}

        expected_params = {"Name": param_name, "WithDecryption": True}

        stub.add_response(
            "get_parameter",
            get_param_response(param_name=param_name),
            expected_params,
        )

        assert handler.get_param(param_name) == expected_response

        handler.logger.info.assert_called_once_with("Getting SSM param", expected_params)

    def test_get_param_raises_on_exception(self, handler, stub):
        param_name = "/OEUR/MyParam"

        expected_params = {"Name": param_name, "WithDecryption": True}

        stub.add_client_error(
            "get_parameter",
            service_error_code="ParameterNotFound",
            service_message=(
                "The parameter couldn't be found." " Verify the name and try again."
            ),
            http_status_code=400,
            expected_params=expected_params,
        )

        with pytest.raises(ClientError) as error:
            handler.get_param(param_name)

        handler.logger.info.assert_called_once_with("Getting SSM param", expected_params)

        handler.logger.error.assert_called_once_with(
            "Failed to get parameter", expected_params, error.value
        )

    def test_get_params_returns_params(self, handler, stub):
        param_names = [
            "/OEUR/MyParam1",
            "/OEUR/MyParam2",
            "/OEUR/MyParam3",
            "/OEUR/MyParam4",
        ]

        expected_response = {
            "/OEUR/MyParam1": "MyValue1",
            "/OEUR/MyParam2": "MyValue2",
            "/OEUR/MyParam3": "MyValue3",
            "/OEUR/MyParam4": "MyValue4",
        }

        expected_params = {"Names": param_names, "WithDecryption": True}

        stub.add_response(
            "get_parameters",
            get_params_response(
                params=[
                    {
                        "Name": key,
                        "Value": value,
                        "Type": "String",
                    }
                    for key, value in expected_response.items()
                ]
            ),
            expected_params=expected_params,
        )

        assert handler.get_params(param_names) == expected_response

        handler.logger.info.assert_called_once_with(
            "Getting SSM params", expected_params
        )

    def test_get_params_raises_on_exception(self, handler, stub):
        param_names = [
            "/OEUR/MyParam1",
            "/OEUR/MyParam2",
            "/OEUR/MyParam3",
            "/OEUR/MyParam4",
        ]

        expected_params = {"Names": param_names, "WithDecryption": True}

        stub.add_client_error(
            "get_parameters",
            service_error_code="InvalidKeyId",
            service_message="The query key ID isn't valid.",
            http_status_code=400,
            expected_params=expected_params,
        )

        with pytest.raises(ClientError) as error:
            handler.get_params(param_names)

        handler.logger.info.assert_called_once_with(
            "Getting SSM params", expected_params
        )

        handler.logger.error.assert_called_once_with(
            "Failed to get parameters", expected_params, error.value
        )
