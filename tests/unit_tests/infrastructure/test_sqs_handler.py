import json
from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.infrastructure import SQSHandler
from tests.unit_tests.factories import sqs_event


class TestSQSHandler:
    @pytest.fixture()
    def handler(self):
        class MockSQSHandler(SQSHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockSQSHandler()

    def test_parse_event_parses_sqs_event(self, handler):
        event = sqs_event(
            body=json.dumps({"Hello": "World"}),
            message_attributes={"MyAttribute", "How'd I Get Here?"},
        )

        handler.event = event

        handler.parse_event()

        assert handler.body == {"Hello": "World"}
        assert handler.message_attributes == {
            "MyAttribute",
            "How'd I Get Here?",
        }

        handler.logger.info.assert_called_once_with("Received Event:", event)

    def test_set_correlation_id_sets_from_message_attributes(self, handler):
        handler.message_attributes = {
            "correlation_id": {"stringValue": "Super-Sick-UUID"}
        }

        handler.set_correlation_id()

        assert handler.correlation_id == "Super-Sick-UUID"

    def test_set_correlation_id_defaults_if_no_message_attributes(self, handler):
        handler.message_attributes = {}

        handler.set_correlation_id()

        assert handler.correlation_id is None
