from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.infrastructure import APIHandler
from tests.unit_tests.factories import api_event


class TestAPIHandler:
    @pytest.fixture()
    def handler(self):
        class MockHandler(APIHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockHandler()

    def test_parse_event_parses_api_event(self, handler):
        handler.event = api_event()

        handler.parse_event()

        assert handler.body == ["2022-06-01T00:00:00Z", "2022-06-02T00:00:00Z"]
