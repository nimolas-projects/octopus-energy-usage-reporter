from datetime import datetime
from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.infrastructure import DateTimeMixin


class TestDateTimeMixin:
    @pytest.fixture()
    def mixin(self):
        class MockedMixin(DateTimeMixin):
            def __init__(self) -> None:
                super().__init__()
                self.logger = MagicMock()

        yield MockedMixin()

    def test_parse_timestamp_returns_datetime(self, mixin):
        epoch_timestamp = 1650153600

        expected_datetime_object = datetime(2022, 4, 17, 00, 00, 00)

        assert mixin.parse_timestamp(epoch_timestamp) == expected_datetime_object

    def test_parse_datetime_string_parses_specific_format_datetimestring(self, mixin):
        datetime_string = "2022-04-17T00:00:00Z"

        expected_datetime_object = datetime(2022, 4, 17, 00, 00, 00)

        assert mixin.parse_datetime_string(datetime_string) == expected_datetime_object

    def test_parse_datetime_string_raises_for_different_format_string(self, mixin):
        datetime_string = "2022-04-17T00: 00: 00Z"

        with pytest.raises(ValueError) as e:
            mixin.parse_datetime_string(datetime_string)

        mixin.logger.error.assert_called_once_with(
            "Incorrect datetime string format", exception=e.value
        )

    def test_get_previous_date_returns_yesterday(self, mixin):
        today = datetime(2022, 4, 17, 0, 0, 0)
        yesterday = datetime(2022, 4, 16, 0, 0, 0)

        assert mixin.subtract_days_from_date(today) == yesterday

    def test_get_previous_date_returns_yesterday_with_no_time_alterations(self, mixin):
        today = datetime(2022, 4, 17, 10, 32, 47)
        yesterday = datetime(2022, 4, 16, 10, 32, 47)

        assert mixin.subtract_days_from_date(today) == yesterday

    def test_get_previous_date_returns_yesterday_when_start_of_month(self, mixin):
        today = datetime(2022, 4, 1, 0, 0, 0)
        yesterday = datetime(2022, 3, 31, 0, 0, 0)

        assert mixin.subtract_days_from_date(today) == yesterday

    def test_get_previous_date_returns_more_than_one_day(self, mixin):
        today = datetime(2022, 3, 31, 0, 0, 0)
        previous_date = datetime(2022, 3, 29, 0, 0, 0)

        assert mixin.subtract_days_from_date(today, 2) == previous_date

    def test_get_time_ranges_returns_time_range_of_date(self, mixin):
        date = datetime(2022, 4, 17, 0, 0, 0)
        start_time = "00:00:00"
        end_time = "23:59:59"

        expected_start = datetime(2022, 4, 17, 0, 0, 0)
        expected_end = datetime(2022, 4, 17, 23, 59, 59)

        assert mixin.get_time_ranges(date, start_time, end_time) == (
            expected_start,
            expected_end,
        )

    def test_get_weekday_string_returns_string(self, mixin):
        indexes_and_days = {
            0: "Monday",
            1: "Tuesday",
            2: "Wednesday",
            3: "Thursday",
            4: "Friday",
            5: "Saturday",
            6: "Sunday",
        }

        for index, day in indexes_and_days.items():
            assert mixin.get_weekday_string(index) == day

    def test_get_weekday_string_returns_monday_if_invalid_index(self, mixin):
        assert mixin.get_weekday_string(7) == "Monday"
        assert mixin.logger.warn.called_once_with(
            "Invalid index passed to get_weekday_string. Returning default",
            {"invalid_index": 7},
        )

    def test_get_month_string_returns_string(self, mixin):
        indexes_and_days = {
            0: "N/A",
            1: "January",
            2: "February",
            3: "March",
            4: "April",
            5: "May",
            6: "June",
            7: "July",
            8: "August",
            9: "September",
            10: "October",
            11: "November",
            12: "December",
        }

        for index, day in indexes_and_days.items():
            assert mixin.get_month_string(index) == day

    def test_get_weekday_string_returns_january_if_invalid_index(self, mixin):
        assert mixin.get_month_string(13) == "January"
        assert mixin.logger.warn.called_once_with(
            "Invalid index passed to get_month_string. Returning default",
            {"invalid_index": 13},
        )

    @pytest.mark.parametrize(
        "start_date,value,expected",
        [
            (
                datetime(2022, 6, 13, 0, 0, 0),
                3,
                datetime(2022, 6, 16, 0, 0, 0),
            ),
            (
                datetime(2022, 6, 13, 0, 0, 0),
                4,
                datetime(2022, 6, 17, 0, 0, 0),
            ),
            (
                datetime(2022, 6, 13, 0, 0, 0),
                5,
                datetime(2022, 6, 18, 0, 0, 0),
            ),
            (
                datetime(2022, 6, 13, 0, 0, 0),
                365,
                datetime(2023, 6, 13, 0, 0, 0),
            ),
        ],
    )
    def test_add_days_to_date_returns_correct_datetime(
        self, mixin, start_date, value, expected
    ):
        assert mixin.add_days_to_date(start_date, value) == expected

    @pytest.mark.parametrize(
        "start_date,end_date,expected",
        [
            (
                datetime(2022, 7, 1, 0, 0, 0),
                datetime(2022, 7, 1, 0, 0, 0),
                [datetime(2022, 7, 1, 0, 0, 0)],
            ),
            (
                datetime(2022, 7, 1, 0, 0, 0),
                datetime(2022, 7, 2, 0, 0, 0),
                [datetime(2022, 7, 1, 0, 0, 0), datetime(2022, 7, 2, 0, 0, 0)],
            ),
            (
                datetime(2022, 7, 1, 0, 0, 0),
                datetime(2022, 7, 3, 0, 0, 0),
                [
                    datetime(2022, 7, 1, 0, 0, 0),
                    datetime(2022, 7, 2, 0, 0, 0),
                    datetime(2022, 7, 3, 0, 0, 0),
                ],
            ),
            (
                datetime(2022, 7, 1, 0, 0, 0),
                datetime(2022, 7, 10, 0, 0, 0),
                [
                    datetime(2022, 7, 1, 0, 0, 0),
                    datetime(2022, 7, 2, 0, 0, 0),
                    datetime(2022, 7, 3, 0, 0, 0),
                    datetime(2022, 7, 4, 0, 0, 0),
                    datetime(2022, 7, 5, 0, 0, 0),
                    datetime(2022, 7, 6, 0, 0, 0),
                    datetime(2022, 7, 7, 0, 0, 0),
                    datetime(2022, 7, 8, 0, 0, 0),
                    datetime(2022, 7, 9, 0, 0, 0),
                    datetime(2022, 7, 10, 0, 0, 0),
                ],
            ),
        ],
    )
    def test_generate_date_range_returns_range_of_datetime(
        self, mixin, start_date, end_date, expected
    ):
        assert list(mixin.generate_date_range(start_date, end_date)) == expected
