import json
import logging as python_logger
import os
from unittest import TestCase

import pytest

from octopus_energy_usage_reporter.infrastructure import Logger

SERVICE_NAME = os.environ.get("SERVICE_NAME")
SOURCE_NAME = os.environ.get("SOURCE_NAME")

logging_path = "octopus_energy_usage_reporter.infrastructure.logger.logging"


class TestLogger:
    @pytest.fixture()
    def make_logger(self):
        def _logger(logging_level):
            class MockLogger(Logger):
                def __init__(self, logging_level):
                    super().__init__(logging_level)

            return MockLogger(logging_level)

        yield _logger

    def test_set_correlation_id_sets_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        logger.set_correlation_id("Super-Sick-UUID")
        assert logger.correlation_id == "Super-Sick-UUID"

    def test_logging_level_affects_log_usage(self, make_logger):
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My log",
            },
            indent=4,
        )

        logger = make_logger(logging_level=python_logger.INFO)
        with TestCase.assertLogs(self) as logs:
            logger.info("My log")
            logger.warn("My log")
            logger.error("My log")

        assert len(logs.records) == 3
        assert (
            all([(log.getMessage() == expected_message) for log in logs.records]) is True
        )

        logger = make_logger(logging_level=python_logger.WARNING)
        with TestCase.assertLogs(self) as logs:
            logger.info("My log")
            logger.warn("My log")
            logger.error("My log")

        assert len(logs.records) == 2
        assert (
            all([(log.getMessage() == expected_message) for log in logs.records]) is True
        )

        logger = make_logger(logging_level=python_logger.ERROR)
        with TestCase.assertLogs(self) as logs:
            logger.info("My log")
            logger.warn("My log")
            logger.error("My log")

        assert len(logs.records) == 1
        assert (
            all([(log.getMessage() == expected_message) for log in logs.records]) is True
        )

    def test_info_logs_with_no_data_and_no_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My info log",
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info("My info log")

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_info_logs_with_data_and_no_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My info log",
                "data": {"Super": "Data"},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info("My info log", {"Super": "Data"})

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_info_logs_with_no_data_and_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My info log",
                "correlation_id": "Super-Sick-UUID",
            },
            indent=4,
        )

        logger.set_correlation_id("Super-Sick-UUID")
        with TestCase.assertLogs(self) as logs:
            logger.info("My info log")

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_info_logs_with_data_and_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My info log",
                "correlation_id": "Super-Sick-UUID",
                "data": {"Super": "Data"},
            },
            indent=4,
        )

        logger.set_correlation_id("Super-Sick-UUID")
        with TestCase.assertLogs(self) as logs:
            logger.info("My info log", {"Super": "Data"})

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_warn_logs_with_no_data_and_no_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.WARNING)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.warn("My warn log")

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_warn_logs_with_data_and_no_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.WARNING)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
                "data": {"Super": "Data"},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.warn("My warn log", {"Super": "Data"})

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_warn_logs_with_no_data_and_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.WARNING)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
                "correlation_id": "Super-Sick-UUID",
            },
            indent=4,
        )

        logger.set_correlation_id("Super-Sick-UUID")
        with TestCase.assertLogs(self) as logs:
            logger.warn("My warn log")

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_warn_logs_with_data_and_correlation_id(self, make_logger):
        logger = make_logger(logging_level=python_logger.WARNING)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
                "correlation_id": "Super-Sick-UUID",
                "data": {"Super": "Data"},
            },
            indent=4,
        )

        logger.set_correlation_id("Super-Sick-UUID")
        with TestCase.assertLogs(self) as logs:
            logger.warn("My warn log", {"Super": "Data"})

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_error_logs_with_no_data_and_no_exception(self, make_logger):
        logger = make_logger(logging_level=python_logger.ERROR)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.error("My warn log")

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_error_logs_with_data_and_no_exception(self, make_logger):
        logger = make_logger(logging_level=python_logger.ERROR)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
                "data": {"Super": "Data"},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.error("My warn log", {"Super": "Data"})

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_error_logs_with_data_and_exception(self, make_logger):
        logger = make_logger(logging_level=python_logger.ERROR)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My warn log",
                "data": {"Super": "Data"},
                "exception": {
                    "type": "<class 'KeyError'>",
                    "error": ["What the key doing?"],
                },
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.error(
                "My warn log",
                {"Super": "Data"},
                KeyError("What the key doing?"),
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_add_redact_keys_can_add_to_redact_keys(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_keys = ["account", "resources", "test_key"]

        logger.add_redact_keys(["test_key"])

        assert logger.redact_keys == expected_keys

    def test_add_redact_keys_cannot_add_allocated_keys(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": (
                    "Pre-existing fields detected. "
                    "These will not be added to redact_keys."
                ),
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.add_redact_keys(
                [
                    "service",
                    "source",
                    "message",
                    "data",
                    "exception",
                ]
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

        assert "service" not in logger.redact_keys
        assert "source" not in logger.redact_keys
        assert "message" not in logger.redact_keys
        assert "data" not in logger.redact_keys
        assert "exception" not in logger.redact_keys

    def test_logs_redact_key_in_redact_keys_in_dict(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {"account": "Redacted 10 character(s)"},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                {"account": "0123456789"},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_logs_redact_key_in_redact_keys_in_list(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {
                    "account": [
                        "Redacted 1 character(s)",
                        "Redacted 2 character(s)",
                        "Redacted 2 character(s)",
                        "Redacted 3 character(s)",
                        "Redacted 3 character(s)",
                    ]
                },
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                {"account": ["I", "Am", "My", "Own", "Log"]},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_logs_redact_key_in_redact_keys_with_num_value(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {"account": "Redacted 9 character(s)"},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                {"account": 123456789},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_logs_can_ignore_keys_not_in_redact_keys(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {
                    "account": "Redacted 9 character(s)",
                    "No-Redact": "Please ignore me",
                },
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                {"account": 123456789, "No-Redact": "Please ignore me"},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_logs_redact_added_keys(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {
                    "account": "Redacted 9 character(s)",
                    "No-Redact": "Redacted 16 character(s)",
                },
            },
            indent=4,
        )

        logger.add_redact_keys(["No-Redact"])
        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                {"account": 123456789, "No-Redact": "Please ignore me"},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_logs_cant_redact_non_dict_data(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": "account: 1234556788",
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                "account: 1234556788",
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_logs_doesnt_redact_numbers_with_no_redact_key(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {"Super Number": 1234},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info(
                "My redact log",
                {"Super Number": 1234},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_error_doesnt_alter_non_exception_objects(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My redact log",
                "data": {"Super Number": 1234},
                "exception": {"Some Error": "AAAAAA"},
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.error(
                "My redact log",
                {"Super Number": 1234},
                {"Some Error": "AAAAAA"},
            )

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    def test_json_parsing_when_default_fails_returns_string(self, make_logger):
        class NonStrClass:
            def __str__(self) -> str:
                raise ValueError("No string here")

        logger = make_logger(logging_level=python_logger.INFO)
        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "My info log",
                "data": "Couldn't parse object",
            },
            indent=4,
        )

        with TestCase.assertLogs(self) as logs:
            logger.info("My info log", NonStrClass())

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message

    # figure out how to test tracebacks - Nyk 16/04/22

    def test_error_returns_traceback_if_present(self, make_logger):
        logger = make_logger(logging_level=python_logger.INFO)

        class ExceptionThrower:
            def __init__(self) -> None:
                self.logger = logger

            def execute(self):
                try:
                    raise KeyError("What the key doing?")
                except KeyError as error:
                    self.logger.error("An error occurred", exception=error)
                    raise

        expected_message = json.dumps(
            {
                "service": SERVICE_NAME,
                "source": SOURCE_NAME,
                "message": "An error occurred",
                "exception": {
                    "type": "<class 'KeyError'>",
                    "error": ["What the key doing?"],
                    "traceback": (
                        (
                            "Traceback (most recent call last):\n"
                            f'  File "{os.path.abspath(__file__)}", '
                            "line 545, in execute\n    "
                            'raise KeyError("What the key doing?")\nKeyError'
                            ": 'What the key doing?'\n"
                        )
                    ),
                },
            },
            indent=4,
        )

        mock_class = ExceptionThrower()

        with TestCase.assertLogs(self) as logs:
            with pytest.raises(KeyError):
                mock_class.execute()

        assert len(logs.records) == 1
        assert logs.records[0].getMessage() == expected_message
