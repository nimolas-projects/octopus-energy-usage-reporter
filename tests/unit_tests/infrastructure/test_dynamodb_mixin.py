from unittest.mock import MagicMock, call

import pytest
from botocore.exceptions import ClientError
from botocore.stub import Stubber

from octopus_energy_usage_reporter.infrastructure import DynamoDBMixin
from tests.unit_tests.factories import put_item_response


class TestDynamoDBMixin:
    @pytest.fixture()
    def mixin(self):
        class MockDynamoDBMixin(DynamoDBMixin):
            def __init__(self) -> None:
                super().__init__()

                self.logger = MagicMock()

        yield MockDynamoDBMixin()

    @pytest.fixture()
    def stub(self):
        from octopus_energy_usage_reporter.infrastructure.dynamodb_mixin import (
            dynamo_client,
        )

        with Stubber(dynamo_client) as stubber:
            yield stubber

            stubber.assert_no_pending_responses()

    def test_put_item_calls_client_put_item(self, mixin, stub):
        formatted_item = {
            "MyString": {"S": "MyValue"},
            "MyInt": {"N": "123"},
            "MyFloat": {"N": "123.123"},
        }

        expected_params = {
            "TableName": "MyTable",
            "Item": formatted_item,
        }

        response = put_item_response()

        stub.add_response("put_item", response, expected_params)

        mixin.put_item(
            "MyTable",
            {"MyString": "MyValue", "MyInt": 123, "MyFloat": 123.123},
        )

        calls = [
            call(
                "Putting item into to DynamoDB",
                {"table_name": "MyTable", "item": formatted_item},
            ),
            call("Response from DynamoDB", response),
        ]

        mixin.logger.info.assert_has_calls(calls)

    def test_put_item_raises_on_exception(self, mixin, stub):
        formatted_item = {
            "MyString": {"S": "MyValue"},
            "MyInt": {"N": "123"},
        }

        expected_params = {
            "TableName": "MyTable",
            "Item": formatted_item,
        }

        payload = {"MyString": "MyValue", "MyInt": 123}

        stub.add_client_error(
            "put_item",
            service_error_code="ResourceNotFoundException",
            service_message=(
                "The operation tried to access a nonexistent table or index.",
                "The resource might not be specified correctly,",
                "or its status might not be ACTIVE.",
            ),
            http_status_code=400,
            expected_params=expected_params,
        )

        with pytest.raises(ClientError) as error:
            mixin.put_item("MyTable", payload)

        mixin.logger.info.assert_called_once_with(
            "Putting item into to DynamoDB",
            {"table_name": "MyTable", "item": formatted_item},
        )

        mixin.logger.error.assert_called_once_with(
            "Error occured when sending to DynamoDB",
            {"payload": payload, "dynamo_item": formatted_item},
            error.value,
        )
