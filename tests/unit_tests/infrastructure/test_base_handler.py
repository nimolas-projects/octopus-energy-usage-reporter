from unittest.mock import MagicMock, call

import pytest

from octopus_energy_usage_reporter.infrastructure import BaseHandler


class TestBaseHandler:
    @pytest.fixture()
    def logger(self):
        mocked_logger = MagicMock()
        yield mocked_logger

    @pytest.fixture()
    def handler(self, logger):
        class MockedHandler(BaseHandler):
            def __init__(self):
                super().__init__()

                self.logger = logger

        yield MockedHandler()

    @pytest.fixture()
    def parseevent_handler(self, logger):
        class MockedHandler(BaseHandler):
            def __init__(self):
                super().__init__()

                self.logger = logger

            def parse_event(self):
                self.new_event = self.event["new_event"]

        yield MockedHandler()

    @pytest.fixture()
    def parsecontext_handler(self, logger):
        class MockedHandler(BaseHandler):
            def __init__(self):
                super().__init__()

                self.logger = logger

            def parse_context(self):
                self.new_context = self.context["new_context"]

        yield MockedHandler()

    def test_parse_event_can_be_overriden(self, parseevent_handler):

        event = {"new_event": ["some", "cool", "event"]}

        parseevent_handler.handler_function(event, None)

        assert parseevent_handler.new_event == event["new_event"]

    def test_parse_context_can_be_overriden(self, parsecontext_handler):

        context = {"new_context": ["some", "cool", "context"]}

        parsecontext_handler.handler_function(None, context)

        assert parsecontext_handler.new_context == context["new_context"]

    def test_handler_function_parses_event_and_context(self, handler):
        handler._set_correlation_id = MagicMock()
        handler.execute = MagicMock()

        expected_event = {"hello": "world"}
        expected_context = {"Super": "Context"}

        handler.handler_function(expected_event, expected_context)

        assert handler.event == expected_event
        assert handler.context == expected_context

    def test_handler_function_sets_correlation_id_if_not_set(self, handler):

        handler.handler_function(None, None)

        assert handler.correlation_id is not None
        handler.logger.set_correlation_id.assert_called_once()

    def test_handler_function_doesnt_override_set_correlation_id(self, handler):
        handler.correlation_id = "Super-Sick-UUID"

        handler.handler_function(None, None)

        handler.logger.set_correlation_id.assert_called_once_with("Super-Sick-UUID")

    def test_handler_function_calls_execute(self, handler):
        handler.execute = MagicMock()

        handler.handler_function(None, None)

        handler.execute.assert_called_once()

    def test_handler_function_returns_ok_response_body(self, handler):
        handler.execute = MagicMock(return_value={"Hello": "World"})

        expected_response = {
            "body": '{"Hello": "World"}',
            "statusCode": 200,
        }

        expected_logger_calls = [
            call("Starting request", {"event": None, "context": None}),
            call("Returning response", expected_response),
        ]

        assert handler.handler_function(None, None) == expected_response

        handler.logger.info.assert_has_calls(expected_logger_calls)

    def test_handler_function_catches_exceptions_and_returns_error_payload(
        self, handler
    ):
        exception = Exception("I'm an exception")
        handler.execute = MagicMock(side_effect=exception)

        assert handler.handler_function(None, None) == {
            "body": "{}",
            "statusCode": 500,
            "exception": exception,
        }

        handler.logger.info.assert_called_once_with(
            "Starting request", {"event": None, "context": None}
        )

        handler.logger.error.assert_called_once_with(
            "Lambda invocation failed", exception=exception
        )
