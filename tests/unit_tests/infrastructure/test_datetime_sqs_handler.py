import json
from datetime import datetime
from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.infrastructure import DateTimeSQSHandler
from tests.unit_tests.factories import sqs_event


class TestDateTimeSQSHandler:
    @pytest.fixture
    def mock_handler(self):
        class MockHandler(DateTimeSQSHandler):
            def __init__(self):
                super().__init__()

                self.logger = MagicMock()

        yield MockHandler()

    def test_parse_event_parses_datetime_strings(self, mock_handler):
        start_date = datetime(2022, 6, 25, 0, 0, 0)
        end_date = datetime(2022, 6, 25, 23, 59, 59)

        event = sqs_event(
            body=json.dumps(
                {
                    "start_date": start_date.isoformat() + "Z",
                    "end_date": end_date.isoformat() + "Z",
                }
            )
        )
        mock_handler.event = event

        mock_handler.parse_event()

        assert mock_handler.start_date == start_date
        assert mock_handler.end_date == end_date

        mock_handler.logger.info.assert_called_once_with("Received Event:", event)

    def test_parse_event_raises_on_invalid_datetime_string(self, mock_handler):
        start_date = datetime(2022, 6, 25, 0, 0, 0)
        end_date = datetime(2022, 6, 25, 23, 59, 59)

        event = sqs_event(
            body=json.dumps(
                {
                    "start_date": start_date.isoformat(),
                    "end_date": end_date.isoformat(),
                }
            )
        )
        mock_handler.event = event

        with pytest.raises(ValueError) as error:
            mock_handler.parse_event()

        mock_handler.logger.error.assert_called_once_with(
            "Incorrect datetime string format", exception=error.value
        )
