from datetime import datetime
from unittest.mock import MagicMock, call

import pytest
import requests
import requests_mock

from octopus_energy_usage_reporter.domain import OctopusAPIMixin
from tests.unit_tests.factories import (
    failed_tariff_charges_response,
    get_consumption_response,
    list_products_response,
    retrieve_product_response,
    successful_tariff_charges_response,
)


class TestOctopusAPIMixin:
    @pytest.fixture
    def mixin(self):
        class MockOctopusAPIMixin(OctopusAPIMixin):
            def __init__(self) -> None:
                super().__init__()

                self.logger = MagicMock()

        yield MockOctopusAPIMixin()

    @pytest.fixture
    def request_mock(self):
        with requests_mock.Mocker() as mock:
            yield mock

    @pytest.fixture
    def url(self):
        from octopus_energy_usage_reporter.domain.octopus_api_mixin import url

        yield url

    @pytest.fixture
    def auth(self):
        yield "Super-Sick-Auth-Bearer-Token"

    @pytest.fixture
    def headers(self, auth):
        yield {"Authorization": f"Basic {auth}"}

    def test_get_product_code_returns_product_code(
        self, mixin, url, request_mock, headers, auth
    ):
        product_name = "MyProductName"
        available_at_date = "2016-01-01T00:00:00Z"

        request_mock.request(
            "GET",
            url + "/products/",
            headers=headers,
            json=list_products_response(),
        )

        assert mixin.get_product_code(auth, product_name, available_at_date) == "1201"

    def test_get_product_code_raises_if_product_name_doesnt_exist(
        self, mixin, url, request_mock, headers, auth
    ):
        product_name = "ThisProductDoesntExist"
        available_at_date = "2016-01-01T00:00:00Z"

        request_mock.request(
            "GET",
            url + "/products/",
            headers=headers,
            json=list_products_response(),
        )

        with pytest.raises(ValueError):
            mixin.get_product_code(auth, product_name, available_at_date)

    def test_get_tariff_code_returns_tariff_code(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = 1201
        register = "single_register_electricity_tariffs"
        tariff_version = "_A"

        request_mock.request(
            "GET",
            url + f"/products/{product_code}",
            headers=headers,
            json=retrieve_product_response(),
        )

        assert (
            mixin.get_tariff_code(auth, product_code, tariff_version, register)
            == "MyTariffCode-A"
        )

    def test_get_tariff_code_raises_if_register_doesnt_exist(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = 1201
        register = "NonExistantRegister"
        tariff_version = "_A"

        request_mock.request(
            "GET",
            url + f"/products/{product_code}",
            headers=headers,
            json=retrieve_product_response(),
        )

        with pytest.raises(ValueError):
            mixin.get_tariff_code(auth, product_code, tariff_version, register)

    def test_get_tariff_code_raises_if_tariff_version_doesnt_exist(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = 1201
        register = "single_register_electricity_tariffs"
        tariff_version = "NonExistantTariffVersion"

        request_mock.request(
            "GET",
            url + f"/products/{product_code}",
            headers=headers,
            json=retrieve_product_response(),
        )

        with pytest.raises(ValueError):
            mixin.get_tariff_code(auth, product_code, tariff_version, register)

    def test_get_tariff_details_returns_tariff_details(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = "1201"
        tariff_code = "MyTariffCode-A"
        available_from_date = "2016-01-01T00:00:00Z"
        available_to_date = "2016-01-02T00:00:00Z"

        api_paths_and_responses = {
            "standing-charges": successful_tariff_charges_response(),
            "standard-unit-rates": successful_tariff_charges_response(),
            "day-unit-rates": successful_tariff_charges_response(),
            "night-unit-rates": successful_tariff_charges_response(),
        }

        for path, response in api_paths_and_responses.items():
            request_mock.request(
                "GET",
                url
                + (
                    f"/products/{product_code}"
                    f"/electricity-tariffs/{tariff_code}/{path}"
                ),
                headers=headers,
                json=response,
            )

        expected_response = {
            key: [{"value_inc_vat": 19.74}] for key, _ in api_paths_and_responses.items()
        }

        assert (
            mixin.get_tariff_details(
                auth,
                product_code,
                tariff_code,
                available_from_date,
                available_to_date,
            )
            == expected_response
        )

    def test_get_tariff_details_returns_mixed_sucessful_and_failed_results(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = "1201"
        tariff_code = "MyTariffCode-A"
        available_from_date = "2016-01-01T00:00:00Z"
        available_to_date = "2016-01-02T00:00:00Z"

        api_paths_and_responses = {
            "standing-charges": successful_tariff_charges_response(),
            "standard-unit-rates": successful_tariff_charges_response(),
            "day-unit-rates": failed_tariff_charges_response(),
            "night-unit-rates": failed_tariff_charges_response(),
        }

        for path, response in api_paths_and_responses.items():
            request_mock.request(
                "GET",
                url
                + (
                    f"/products/{product_code}"
                    f"/electricity-tariffs/{tariff_code}/{path}"
                ),
                headers=headers,
                json=response,
            )

        expected_response = {
            "standing-charges": [{"value_inc_vat": 19.74}],
            "standard-unit-rates": [{"value_inc_vat": 19.74}],
            "day-unit-rates": "N/A",
            "night-unit-rates": "N/A",
        }

        assert (
            mixin.get_tariff_details(
                auth,
                product_code,
                tariff_code,
                available_from_date,
                available_to_date,
            )
            == expected_response
        )

    def test_get_tariff_details_doesnt_raise_if_expected_error_code(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = "1201"
        tariff_code = "MyTariffCode-A"
        available_from_date = "2016-01-01T00:00:00Z"
        available_to_date = "2016-01-02T00:00:00Z"

        api_paths_and_responses = {
            "standing-charges": failed_tariff_charges_response(),
            "standard-unit-rates": failed_tariff_charges_response(),
            "day-unit-rates": failed_tariff_charges_response(),
            "night-unit-rates": failed_tariff_charges_response(),
        }

        for path, response in api_paths_and_responses.items():
            request_mock.request(
                "GET",
                url
                + (
                    f"/products/{product_code}"
                    f"/electricity-tariffs/{tariff_code}/{path}"
                ),
                headers=headers,
                json=response,
                status_code=400,
            )

        expected_response = {
            "standing-charges": "N/A",
            "standard-unit-rates": "N/A",
            "day-unit-rates": "N/A",
            "night-unit-rates": "N/A",
        }

        assert (
            mixin.get_tariff_details(
                auth,
                product_code,
                tariff_code,
                available_from_date,
                available_to_date,
            )
            == expected_response
        )

    def test_get_tariff_details_raises_if_non_expected_error_code(
        self, mixin, url, request_mock, headers, auth
    ):
        product_code = "1201"
        tariff_code = "MyTariffCode-A"
        available_from_date = "2016-01-01T00:00:00Z"
        available_to_date = "2016-01-02T00:00:00Z"

        api_paths_and_responses = {
            "standing-charges": successful_tariff_charges_response(),
        }

        for path, response in api_paths_and_responses.items():
            request_mock.request(
                "GET",
                url
                + (
                    f"/products/{product_code}"
                    f"/electricity-tariffs/{tariff_code}/{path}"
                ),
                headers=headers,
                json=response,
                status_code=404,
            )

        with pytest.raises(requests.exceptions.HTTPError):
            mixin.get_tariff_details(
                auth,
                product_code,
                tariff_code,
                available_from_date,
                available_to_date,
            )

    def test_get_tariff_prices_returns_prices(self, mixin, auth):
        product_name = "MyProductName"
        tariff_version = "MyTariffVersion"
        product_date = datetime(2022, 1, 1, 0, 0, 0).isoformat()
        register = "MyRegister"
        available_from_date = datetime(2022, 6, 19, 0, 0, 0).isoformat()
        available_to_date = datetime(2022, 6, 20, 0, 0, 0).isoformat()
        product_code = "MyProductCode"
        tariff_code = "MyTariffCode"

        expected_response = {
            "standing-charges": [{"value_inc_vat": 19.74}],
            "standard-unit-rates": [{"value_inc_vat": 19.74}],
            "day-unit-rates": [{"value_inc_vat": 19.74}],
            "night-unit-rates": [{"value_inc_vat": 19.74}],
        }

        mixin.get_product_code = MagicMock(return_value=product_code)
        mixin.get_tariff_code = MagicMock(return_value=tariff_code)
        mixin.get_tariff_details = MagicMock(return_value=expected_response)

        [
            call(
                "Getting product code",
                {
                    "product_name": product_name,
                    "tariff_version": tariff_version,
                    "product_date": product_date,
                    "register": register,
                    "available_from": available_from_date,
                    "available_to": available_to_date,
                },
            ),
            call("Getting tariff code", {"product_code": product_code}),
            call("Getting tariff details", {"tariff_code": tariff_code}),
        ]

        assert (
            mixin.get_tariff_prices(
                auth,
                product_name,
                tariff_version,
                product_date,
                register,
                available_from_date,
                available_to_date,
            )
            == expected_response
        )

        mixin.get_product_code.assert_called_once_with(auth, product_name, product_date)
        mixin.get_tariff_code.assert_called_once_with(
            auth, product_code, tariff_version, register
        )
        mixin.get_tariff_details.assert_called_once_with(
            auth,
            product_code,
            tariff_code,
            available_from_date,
            available_to_date,
        )

    def test_get_consumption_returns_consumption(
        self, mixin, url, request_mock, headers, auth
    ):
        mban = "MyMBAN"
        serial_number = "MySerialNumber"
        period_from = datetime(2022, 6, 19, 0, 0, 0).isoformat()
        period_to = datetime(2022, 6, 20, 0, 0, 0).isoformat()

        request_mock.request(
            "GET",
            url
            + (
                f"/electricity-meter-points/{mban}"
                f"/meters/{serial_number}/consumption/"
            ),
            headers=headers,
            json=get_consumption_response(),
        )

        assert (
            mixin.get_meter_consumption(
                auth, mban, serial_number, period_from, period_to
            )
            == 8.705
        )

        mixin.logger.info.assert_called_once_with(
            "Getting meter consumption",
            {
                "mban": mban,
                "serial_number": serial_number,
                "period_from": period_from,
                "period_to": period_to,
            },
        )
