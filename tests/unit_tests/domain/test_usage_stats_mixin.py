from unittest.mock import MagicMock

import pytest

from octopus_energy_usage_reporter.domain import UsageStatsMixin
from tests.unit_tests.factories import parse_weekday_query_items, parsed_query_items


class TestUsageStatsMixin:
    @pytest.fixture()
    def mixin(self):
        class MockedMixin(UsageStatsMixin):
            def __init__(self) -> None:
                super().__init__()
                self.logger = MagicMock()

        yield MockedMixin()

    def test_get_stats_for_collection_returns_stats_of_dataset(self, mixin):
        expected_result = {
            "costings": {
                "stats": {
                    "day": {
                        "max": 4.06,
                        "mean": 2.5967,
                        "median": 2.63,
                        "min": 1.1,
                    },
                    "night": {
                        "max": 1.45,
                        "mean": 0.9233,
                        "median": 0.84,
                        "min": 0.48,
                    },
                },
                "values": {
                    "day": [
                        {"value": 2.63, "timestamp": 1657929600},
                        {"value": 4.06, "timestamp": 1658016000},
                        {"value": 1.1, "timestamp": 1658102400},
                    ],
                    "night": [
                        {"value": 1.45, "timestamp": 1657929600},
                        {"value": 0.84, "timestamp": 1658016000},
                        {"value": 0.48, "timestamp": 1658102400},
                    ],
                },
            },
            "usage": {
                "stats": {
                    "day": {
                        "max": 13.101,
                        "min": 2.374,
                        "median": 7.91,
                        "mean": 7.795,
                    },
                    "night": {
                        "max": 3.656,
                        "min": 0.12,
                        "median": 1.44,
                        "mean": 1.7387,
                    },
                },
                "values": {
                    "day": [
                        {"value": 7.91, "timestamp": 1657929600},
                        {"value": 13.101, "timestamp": 1658016000},
                        {"value": 2.374, "timestamp": 1658102400},
                    ],
                    "night": [
                        {"value": 3.656, "timestamp": 1657929600},
                        {"value": 1.440, "timestamp": 1658016000},
                        {"value": 0.120, "timestamp": 1658102400},
                    ],
                },
            },
        }

        assert mixin.get_stats_for_collection(parsed_query_items()) == expected_result

    def test_get_stats_for_weekdays_returns_of_dataset(self, mixin):
        dataset = {}

        for weekday_data in parse_weekday_query_items():
            weekday = weekday_data["WeekDay"]

            dataset[weekday] = (
                [*dataset[weekday], weekday_data]
                if weekday in dataset
                else [weekday_data]
            )

        expected_result = {
            "Saturday": {
                "costings": {
                    "stats": {
                        "day": {
                            "max": 2.63,
                            "mean": 2.63,
                            "median": 2.63,
                            "min": 2.63,
                        },
                        "night": {
                            "max": 1.45,
                            "mean": 1.45,
                            "median": 1.45,
                            "min": 1.45,
                        },
                    },
                    "values": {
                        "day": [
                            {"value": 2.63, "timestamp": 1657929600},
                            {"value": 2.63, "timestamp": 1657929600},
                        ],
                        "night": [
                            {"value": 1.45, "timestamp": 1657929600},
                            {"value": 1.45, "timestamp": 1657929600},
                        ],
                    },
                },
                "usage": {
                    "stats": {
                        "day": {
                            "max": 7.91,
                            "min": 7.91,
                            "median": 7.91,
                            "mean": 7.91,
                        },
                        "night": {
                            "max": 3.656,
                            "min": 3.656,
                            "median": 3.656,
                            "mean": 3.656,
                        },
                    },
                    "values": {
                        "day": [
                            {"value": 7.91, "timestamp": 1657929600},
                            {"value": 7.91, "timestamp": 1657929600},
                        ],
                        "night": [
                            {"value": 3.656, "timestamp": 1657929600},
                            {"value": 3.656, "timestamp": 1657929600},
                        ],
                    },
                },
            },
            "Sunday": {
                "costings": {
                    "stats": {
                        "day": {
                            "max": 4.06,
                            "mean": 4.06,
                            "median": 4.06,
                            "min": 4.06,
                        },
                        "night": {
                            "max": 0.84,
                            "mean": 0.84,
                            "median": 0.84,
                            "min": 0.84,
                        },
                    },
                    "values": {
                        "day": [
                            {"value": 4.06, "timestamp": 1658016000},
                            {"value": 4.06, "timestamp": 1658016000},
                        ],
                        "night": [
                            {"value": 0.84, "timestamp": 1658016000},
                            {"value": 0.84, "timestamp": 1658016000},
                        ],
                    },
                },
                "usage": {
                    "stats": {
                        "day": {
                            "max": 13.101,
                            "min": 13.101,
                            "median": 13.101,
                            "mean": 13.101,
                        },
                        "night": {
                            "max": 1.440,
                            "min": 1.440,
                            "median": 1.440,
                            "mean": 1.440,
                        },
                    },
                    "values": {
                        "day": [
                            {"value": 13.101, "timestamp": 1658016000},
                            {"value": 13.101, "timestamp": 1658016000},
                        ],
                        "night": [
                            {"value": 1.440, "timestamp": 1658016000},
                            {"value": 1.440, "timestamp": 1658016000},
                        ],
                    },
                },
            },
        }

        assert mixin.get_stats_for_weekdays(dataset) == expected_result
