# Octopus Energy Usage Reporter

This repo uses [AWS](https://aws.amazon.com/) and the [Octopus Energy API](https://developer.octopus.energy/docs/api/) to generate a monthly report of a given Octopus Energy user's electricity (kWh) usage. The reports are generated using [Python](https://www.python.org/), specifically [Matplotlib](https://pypi.org/project/matplotlib/).

## Requirements

Below are the requirements for forking this repo, as well as utilising the project in AWS so that reports can be generated.

### Dev Requirements

If you do not wish to edit your forked repo, you can skip this section.

Below are the software/packages that you need if you wish to edit the code of your forked repo:

 - [Python](https://www.python.org/)
 - [Pyenv](https://github.com/pyenv/pyenv)
 - [Poetry](https://python-poetry.org/)
 - [WSL](https://docs.microsoft.com/en-us/windows/wsl/install) (Only required for Windows users. Will make using the commands for the above easier)
 - [Just](https://github.com/casey/just)

 Once the above is setup, you can run the command below to download all the dependancies and setup the development environment.

 ```
 just local
 ```

 ### Deployment Requirements

 The below is required for everyone that wishes to deploy the CloudFormation stack to AWS.

  - An [AWS IAM role](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user.html) for the GitLab pipeline to deploy to AWS. Add the below values after creating the role to the[ GitLab variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) in the following format:
    - AWS_ACCESS_KEY_ID: [Value from AWS]
    - AWS_DEFAULT_REGION: [Value from AWS]
    - AWS_SECRET_ACCESS_KEY: [Value from AWS]
  - Add the following parameters to [Systems Manager in AWS](https://docs.aws.amazon.com/systems-manager/latest/userguide/parameter-create-console.html):
    - /OEUR/APIAuth
        - The value of this on the developer page isn't what the parameter value should be. Testing with API software such as [Postman](https://www.postman.com/) to the Octopus Energy API to get the value is required.
    - /OEUR/MBAN
        - Meter Point Administration Number. Identifies the specific electricity supply point for your home.
    - /OEUR/SerialNumber
        - The serial number of the smart meter in your home.
        - These 3 above can be obtained from the [developer page](https://octopus.energy/dashboard/developer/) of the Octopus Energy my account page.
    - /OEUR/ProductName
        - The name of the electricity product you are currently using with Octopus Energy. This can be found on the dashboard of the My Account page on Octopus Energy's website
    - /OEUR/TariffVersion
        - This value will need to be found with the API. This can be found from the[ retrieve a product](https://developer.octopus.energy/docs/api/#retrieve-a-product) endpoint. These values normally start with a `_`. Example: `_A`.
    - /OEUR/ProductStartDate
        - This value will need to be found with the API. This can be found from the[ retrieve a product](https://developer.octopus.energy/docs/api/#retrieve-a-product) endpoint. First you will need to find the product name to search for this using the [list products](https://developer.octopus.energy/docs/api/#list-products) endpoint. This value is the `available_from` key from the response.
    - /OEUR/ElectricityRegister
        - This value will need to be found with the API. This can be found from the[ retrieve a product](https://developer.octopus.energy/docs/api/#retrieve-a-product) endpoint. This value with either be `single_register_electricity_tariffs` or `dual_register_electricity_tariffs` depending on which one has values for your product.
    - /OEUR/E-Mail
        - This is the E-Mail that reports will be sent to. Also used for alarms for DLQs


## Usage

The general usage of this AWS Cloudformation stack, is that of a passive one. However, there are manual endpoints if you so require to add data or generate reports ad-hoc.

### /date-range endpoint

This endpoint is used to add date ranges to the service from previous dates. 

This uses all the current settings that are sent in SSM, so if your Octopus Energy tariffs change please update these values accordingly before using the endpoint.

**Params**

N/A

**Body**

An array of two dates. The first being the start date and the last being the end date,  inclusive.

(If you use the same date, for both values you'll add 1 date)

```json
[
    "2022-06-01T00:00:00Z",
    "2022-06-30T00:00:00Z"
]
```

## Architecture Diagram

![Octopus Energy Usage Reporter AWS architecture diagram](architecture.png "Architecture Diagram")
