"""
Handler to deal with manual date additions.

Range of dates added to the database using the current config.
"""

import os

from infrastructure import APIHandler, DateTimeMixin, SQSSendMessageMixin

SQS_QUEUE = os.environ.get("SQS_QUEUE")


class DateRangeHandler(APIHandler, DateTimeMixin, SQSSendMessageMixin):
    """
    Handler to deal with manual date additions.

    Range of dates added to the database using the current config.
    """

    def __init__(self):
        """Initalise all inherited classes."""
        super().__init__()

    def parse_event(self):
        """Parse the dates of the api event.

        Args:
            - None

        Returns:
            - None
        """
        super().parse_event()

        self.start_date = self.parse_datetime_string(self.body[0])
        self.end_date = self.parse_datetime_string(self.body[1])

    def execute(self):
        """Generate dates from event, and send to queue.

        Args:
            - None

        Return:
            - None
        """
        self.logger.info(
            "Date range from the api event",
            {"start_date": self.start_date, "end_date": self.end_date},
        )

        dates = []

        for date in self.generate_date_range(self.start_date, self.end_date):
            start_datetime, end_datetime = self.get_time_ranges(
                date, "00:00:00", "23:59:59"
            )

            sqs_payload = {
                "queue_url": SQS_QUEUE,
                "message_body": {
                    "start_date": start_datetime.isoformat() + "Z",
                    "end_date": end_datetime.isoformat() + "Z",
                },
                "message_attributes": {"correlation_id": self.correlation_id},
            }

            self.send_message_to_sqs(**sqs_payload)

            dates.append(date.isoformat())

        return {
            "message": f"Sent {len(dates)} dates to SQS Queue for processing",
            "dates": dates,
        }


handler = DateRangeHandler().handler_function
