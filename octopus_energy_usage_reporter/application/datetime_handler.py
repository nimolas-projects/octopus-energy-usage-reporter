"""Handler that controls DateTime periods."""

import os

from infrastructure import BaseHandler, DateTimeMixin, SQSSendMessageMixin

SQS_QUEUE = os.environ.get("SQS_QUEUE")


class DateTimeHandler(BaseHandler, DateTimeMixin, SQSSendMessageMixin):
    """Handler that generates DateTimes to poll Octopus API with."""

    def __init__(self):
        """Initialise inheritied classes."""
        super().__init__()

    def parse_event(self):
        """Parse the time of the cron event.

        Args:
            - None

        Returns:
            - None
        """
        self.cron_time = self.parse_datetime_string(self.event["time"])

    def execute(self):
        """Get timeframe from cron job, and send to queue.

        Args:
            - None

        Return:
            - None
        """
        self.logger.info("Time from the cron job", {"time": self.cron_time})

        previous_date = self.subtract_days_from_date(self.cron_time, 2)

        start_datetime, end_datetime = self.get_time_ranges(
            previous_date, "00:00:00", "23:59:59"
        )

        sqs_payload = {
            "queue_url": SQS_QUEUE,
            "message_body": {
                "start_date": start_datetime.isoformat() + "Z",
                "end_date": end_datetime.isoformat() + "Z",
            },
            "message_attributes": {"correlation_id": self.correlation_id},
        }

        self.send_message_to_sqs(**sqs_payload)


handler = DateTimeHandler().handler_function
