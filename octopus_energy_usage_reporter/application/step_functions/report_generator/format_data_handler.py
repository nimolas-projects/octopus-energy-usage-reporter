"""Handler that deals with formatting the data from DynamoDB for graphs."""

from domain import UsageStatsMixin
from infrastructure import (
    DateTimeMixin,
    DynamoDBSerialisationMixin,
    StepFunctionStateHandler,
)


class FormatDataHandler(
    StepFunctionStateHandler,
    DateTimeMixin,
    DynamoDBSerialisationMixin,
    UsageStatsMixin,
):
    """Handler class for formatting data."""

    def parse_event(self):
        """Parse the results from the DynamoDB map."""
        super().parse_event()

        self.monthly_data = []
        self.weekday_data = {}

        for item in self.body:
            parsed_response = self.parse_dynamodb_response(item)
            weekday = parsed_response["WeekDay"]

            self.monthly_data.append(parsed_response)

            self.weekday_data[weekday] = (
                [*self.weekday_data[weekday], parsed_response]
                if weekday in self.weekday_data
                else [parsed_response]
            )

        self.month = self.monthly_data[0]["Month"]
        self.year = self.parse_timestamp(self.monthly_data[0]["DateEpoch"]).year

    def _four_stats_pie_chart(self, data, type_of_stat, period):
        return {
            "s3_file_name": (
                f"{self.year}/{self.month}/{type_of_stat}" f"_stats_for_{period}"
            ),
            "type": "pie_graph",
            "graph_data": {
                "data": [
                    {
                        "values": [day_value for day_value in value.values()],
                        "title": key,
                        "legend_title": "Weekdays",
                        "labels": [label for label in value.keys()],
                    }
                    for key, value in data.items()
                ],
                "plot_rows": 2,
                "plot_columns": 2,
            },
        }

    def _two_stacked_bar_chart(
        self, data, type_of_stat, period, x_label="X", y_label="Y"
    ):
        return {
            "s3_file_name": (
                f"{self.year}/{self.month}/{type_of_stat}" f"_by_day_for_{period}"
            ),
            "type": "bar_graph",
            "graph_data": {
                "data": [
                    {
                        "values": [day["value"] for day in time_period],
                        "labels": [
                            self.parse_timestamp(day["timestamp"]).day
                            for day in time_period
                        ],
                    }
                    for time_period in data.values()
                ],
                "title": f"{type_of_stat} by day for {period}",
                "x_label": x_label,
                "y_label": y_label,
                "legends": [legend for legend in data.keys()],
            },
        }

    def _sort_by_stats_monthly(self, data, type_of_stat):
        stats = ["max", "min", "mean", "median"]

        return {
            stat: {
                key: value[stat] for key, value in data[type_of_stat]["stats"].items()
            }
            for stat in stats
        }

    def _sort_by_stats_weekday(self, data, type_of_stat, period):
        stats = ["max", "min", "mean", "median"]

        return {
            stat: {
                key: value[type_of_stat]["stats"][period][stat]
                for key, value in data.items()
            }
            for stat in stats
        }

    def create_graphs(self, monthly_data, weekday_data: dict):
        """Create all graphs for storage in S3."""
        cost_weekday_day_stats = self._sort_by_stats_weekday(
            weekday_data, "costings", "day"
        )
        cost_weekday_night_stats = self._sort_by_stats_weekday(
            weekday_data, "costings", "night"
        )
        usage_weekday_day_stats = self._sort_by_stats_weekday(
            weekday_data, "usage", "day"
        )
        usage_weekday_night_stats = self._sort_by_stats_weekday(
            weekday_data, "usage", "night"
        )

        cost_monthly_stats = self._sort_by_stats_monthly(monthly_data, "costings")
        usage_monthly_stats = self._sort_by_stats_monthly(monthly_data, "usage")

        cost_daily_values = monthly_data["costings"]["values"]
        usage_daily_values = monthly_data["usage"]["values"]

        return [
            self._four_stats_pie_chart(cost_weekday_day_stats, "costings", "day"),
            self._four_stats_pie_chart(cost_weekday_night_stats, "costings", "night"),
            self._four_stats_pie_chart(usage_weekday_day_stats, "usage", "day"),
            self._four_stats_pie_chart(usage_weekday_night_stats, "usage", "night"),
            self._four_stats_pie_chart(cost_monthly_stats, "cost", "month"),
            self._four_stats_pie_chart(usage_monthly_stats, "usage", "month"),
            self._two_stacked_bar_chart(
                cost_daily_values, "cost", self.month, "Dates", "£"
            ),
            self._two_stacked_bar_chart(
                usage_daily_values, "usage", self.month, "Days", "kWh"
            ),
        ]

    def execute(self):
        """Format data for use in graphs."""
        self.logger.info(
            "Generating stats from DynamoDB Data",
            {
                "monthly_data": self.monthly_data,
                "weekday_data": self.weekday_data,
            },
        )

        monthly_data = self.get_stats_for_collection(self.monthly_data)

        weekday_data = self.get_stats_for_weekdays(self.weekday_data)

        self.logger.info("Generating graph data", {**monthly_data, **weekday_data})

        return self.create_graphs(monthly_data, weekday_data)


handler = FormatDataHandler().handler_function
