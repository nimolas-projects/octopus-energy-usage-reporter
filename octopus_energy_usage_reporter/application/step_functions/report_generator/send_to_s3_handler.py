"""Handler that deals with sending graphs to S3."""
import os
from base64 import b64decode
from zlib import decompress

from infrastructure import S3Mixin, StepFunctionStateHandler

BUCKET_NAME = os.environ.get("BUCKET_NAME")


class SendToS3Handler(StepFunctionStateHandler, S3Mixin):
    """Handler class for parsing graph data and sending to S3."""

    def parse_event(self):
        """Parse the event from the create graphs step."""
        super().parse_event()

        self.file_name = self.body["file_name"]
        self.file_data = decompress(b64decode(self.body["file_data"].encode()))

    def execute(self):
        """Send the graph to S3."""
        s3_object = {
            "Bucket": BUCKET_NAME,
            "Key": self.file_name,
            "Body": self.file_data,
        }

        self.put_object(s3_object)

        return "/".join(self.file_name.split("/")[0:2])


handler = SendToS3Handler().handler_function
