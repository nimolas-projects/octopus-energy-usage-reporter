"""Handler that deals with parsing and returning dates for DynamoDB queries."""
from datetime import datetime

from infrastructure import DateTimeMixin, StepFunctionStateHandler


class ParseDatesHandler(StepFunctionStateHandler, DateTimeMixin):
    """Handler class for parsing dates from SQS."""

    def parse_event(self):
        """Parse dates from event."""
        self.start_date = self.parse_datetime_string(self.event["start_date"])
        self.end_date = self.parse_datetime_string(self.event["end_date"])

    def format_date(self, date: datetime):
        """Format the date for DynamoDB query."""
        return {
            "Month": self.get_month_string(date.month),
            "Date": str(date.timestamp()),
            "Day": self.get_weekday_string(date.weekday()),
        }

    def execute(self):
        """Generate and send dates to Dynamodb map."""
        self.logger.info("Parsing event and generating dates", self.event)

        return [
            self.format_date(date)
            for date in self.generate_date_range(self.start_date, self.end_date)
        ]


handler = ParseDatesHandler().handler_function
