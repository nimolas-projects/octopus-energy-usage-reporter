"""Handler that deals with creating graphs in the step function."""
from base64 import b64encode
from io import BytesIO
from zlib import compress

from infrastructure import GraphMixin, StepFunctionStateHandler


class CreateGraphHandler(StepFunctionStateHandler, GraphMixin):
    """Handler class for creating graphs."""

    def __init__(self):
        """Initialise functions used for creating graphs."""
        super().__init__()

        self.graph_type_functions = {
            "pie_graph": self.create_pie_graph,
            "bar_graph": self.create_bar_graph,
        }

    def _create_graph(self, graph_data) -> BytesIO:
        return self.graph_type_functions[graph_data["type"]](
            graph_data["s3_file_name"], **graph_data["graph_data"]
        )

    def execute(self):
        """Create and return objects of graph data."""
        graph_result = self._create_graph(self.body)

        file_data = b64encode(compress(graph_result.getvalue())).decode()

        return {
            "file_name": f"{self.body['s3_file_name']}.png",
            "file_data": file_data,
        }


handler = CreateGraphHandler().handler_function
