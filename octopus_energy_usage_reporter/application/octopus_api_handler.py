"""Handler that controls talking to Octopus API."""

import os

from domain import OctopusAPIMixin
from infrastructure import DateTimeSQSHandler, DynamoDBMixin, SSMParamMixin

TABLE_NAME = os.environ.get("TABLE_NAME")


class OctopusAPIHandler(
    DateTimeSQSHandler,
    OctopusAPIMixin,
    SSMParamMixin,
    DynamoDBMixin,
):
    """Handler that polls Octopus API and stores results in DynamoDB."""

    def _get_kwh_rate(self, tariff_details, time_of_day):
        return (
            tariff_details["standard-unit-rates"][0]["value_inc_vat"]
            if tariff_details["standard-unit-rates"] != "N/A"
            else tariff_details[time_of_day][0]["value_inc_vat"]
        )

    def _get_consumption(self, params, start_date, end_date):
        return self.get_meter_consumption(
            params["/octopus-energy-usage-reporter/APIAuth"],
            params["/octopus-energy-usage-reporter/MBAN"],
            params["/octopus-energy-usage-reporter/SerialNumber"],
            start_date.isoformat(),
            end_date.isoformat(),
        )

    def execute(self):
        """Get SQS event and place relevant data in DyanmoDB.

        Args:
            - None

        Return:
            - None
        """
        param_names = [
            "/octopus-energy-usage-reporter/APIAuth",
            "/octopus-energy-usage-reporter/ProductName",
            "/octopus-energy-usage-reporter/TariffVersion",
            "/octopus-energy-usage-reporter/ProductStartDate",
            "/octopus-energy-usage-reporter/ElectricityRegister",
            "/octopus-energy-usage-reporter/MBAN",
            "/octopus-energy-usage-reporter/SerialNumber",
        ]

        params = self.get_params(param_names)

        tariff_details = self.get_tariff_prices(
            params["/octopus-energy-usage-reporter/APIAuth"],
            params["/octopus-energy-usage-reporter/ProductName"],
            params["/octopus-energy-usage-reporter/TariffVersion"],
            params["/octopus-energy-usage-reporter/ProductStartDate"],
            params["/octopus-energy-usage-reporter/ElectricityRegister"],
            self.start_date.isoformat(),
            self.end_date.isoformat(),
        )

        self.logger.info("Tariff details", tariff_details)

        night_start_date, night_end_date = self.get_time_ranges(
            self.start_date, "00:00:00", "06:59:59"
        )

        day_start_date, day_end_date = self.get_time_ranges(
            self.start_date, "07:00:00", "23:59:59"
        )

        night_consumption = self._get_consumption(
            params, night_start_date, night_end_date
        )
        day_consumption = self._get_consumption(params, day_start_date, day_end_date)

        day_rate = self._get_kwh_rate(tariff_details, "day-unit-rates")
        night_rate = self._get_kwh_rate(tariff_details, "night-unit-rates")
        standing_charge = tariff_details["standing-charges"][0]["value_inc_vat"]

        expiration = self.add_days_to_date(self.start_date, 365 * 2).timestamp()

        weekday = self.get_weekday_string(self.start_date.weekday())

        self.put_item(
            TABLE_NAME,
            {
                "Month": self.start_date.strftime("%B"),
                "DateEpoch": self.start_date.timestamp(),
                "NightkWh": night_consumption,
                "DaykWh": day_consumption,
                "DayRate": day_rate,
                "NightRate": night_rate,
                "StandingCharge": standing_charge,
                "NightStartHour": "00:00",
                "NightEndHour": "07:00",
                "WeekDay": weekday,
                "Expire": expiration,
            },
        )


handler = OctopusAPIHandler().handler_function
