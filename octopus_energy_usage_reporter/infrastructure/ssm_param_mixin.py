"""Handler that deals with SSM."""

import boto3
from botocore.exceptions import ClientError

ssm_client = boto3.client("ssm", region_name="eu-west-2")


class SSMParamMixin:
    """Handler that deals with SSM."""

    def get_param(self, param_name):
        """Get single param from SSM.

        Args:
            - param_name: str

        Return:
            - dict
        """
        try:
            func_params = {"Name": param_name, "WithDecryption": True}
            self.logger.info("Getting SSM param", func_params)

            response = ssm_client.get_parameter(**func_params)

            return {
                "Name": response["Parameter"]["Name"],
                "Value": response["Parameter"]["Value"],
            }
        except ClientError as e:
            self.logger.error("Failed to get parameter", func_params, e)
            raise

    def get_params(self, param_names):
        """Get multiple params from SSM.

        Args:
            - param_names: List[str]

        Return:
            - dict
        """
        try:
            func_params = {"Names": param_names, "WithDecryption": True}
            self.logger.info("Getting SSM params", func_params)

            response = ssm_client.get_parameters(**func_params)

            return {param["Name"]: param["Value"] for param in response["Parameters"]}
        except ClientError as e:
            self.logger.error("Failed to get parameters", func_params, e)
            raise
