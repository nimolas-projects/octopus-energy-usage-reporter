"""Module used for putting DynamoDB items."""

import boto3
from botocore.exceptions import ClientError

from .dynamodb_serialisation_mixin import DynamoDBSerialisationMixin

dynamo_client = boto3.client("dynamodb", region_name="eu-west-2")


class DynamoDBMixin(DynamoDBSerialisationMixin):
    """Mixin for putting DyanmoDB items."""

    def _serialise_dynamo_type(self, value):
        return (
            "S"
            if isinstance(value, str)
            else "N"
            if isinstance(value, int) or isinstance(value, float)
            else "?"
        )

    def _format_dynamo_items(self, payload):
        return {
            key: {self._serialise_dynamo_type(value): str(value)}
            for key, value in payload.items()
        }

    def put_item(self, table_name, payload):
        """Put item in DynamoDB.

        Args:
            - payload: dict

        Returns:
            - None
        """
        formatted_dynamo_items = self._format_dynamo_items(payload)

        self.logger.info(
            "Putting item into to DynamoDB",
            {"table_name": table_name, "item": formatted_dynamo_items},
        )

        try:
            response = dynamo_client.put_item(
                TableName=table_name, Item=formatted_dynamo_items
            )
            self.logger.info("Response from DynamoDB", response)
        except ClientError as e:
            self.logger.error(
                "Error occured when sending to DynamoDB",
                {"payload": payload, "dynamo_item": formatted_dynamo_items},
                e,
            )
            raise
