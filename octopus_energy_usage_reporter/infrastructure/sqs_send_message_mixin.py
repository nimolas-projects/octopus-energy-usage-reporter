"""Module used for sending SQS messages."""

import json

import boto3
from botocore.exceptions import ClientError

sqs_client = boto3.client("sqs", region_name="eu-west-2")


class SQSSendMessageMixin:
    """Mixin for sending SQS messages."""

    def _get_value_type(self, value):
        return (
            "String"
            if isinstance(value, str)
            else "Number"
            if isinstance(value, int) or isinstance(value, float)
            else "?"
        )

    def _create_message_attributes(self, message_attributes):
        return {
            key: {
                "StringValue": str(value),
                "DataType": self._get_value_type(value),
            }
            for key, value in message_attributes.items()
        }

    def _create_sqs_payload(self, queue_url, message_body, message_attributes):
        return {
            "QueueUrl": queue_url,
            "MessageBody": json.dumps(message_body),
            "MessageAttributes": self._create_message_attributes(message_attributes),
        }

    def send_message_to_sqs(self, queue_url, message_body, message_attributes):
        """Send message to SQS.

        Args:
            - payload: dict

        Returns:
            - None
        """
        payload = self._create_sqs_payload(queue_url, message_body, message_attributes)

        self.logger.info("Sending payload to SQS", payload)

        try:
            response = sqs_client.send_message(**payload)
            self.logger.info("Response from SQS", response)
        except ClientError as e:
            self.logger.error("Error occured when sending to SQS", payload, e)
            raise
