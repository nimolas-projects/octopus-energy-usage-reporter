"""Mixin that uses MatplotLib to make graphs."""

from io import BytesIO

import matplotlib.pyplot as plot


class GraphMixin:
    """Mixin to make graphs from DynamoDB data."""

    def create_pie_graph(
        self,
        graph_name,
        data: list[dict],
        plot_rows=1,
        plot_columns=1,
        squeeze=False,
    ):
        """Create pie graph from provided data."""
        fig, axes = plot.subplots(
            nrows=plot_rows,
            ncols=plot_columns,
            squeeze=squeeze,
            figsize=(10, 10),
        )

        data_index = 0
        for row_index in range(plot_rows):
            for column_index in range(plot_columns):
                graph_data = data[data_index]

                wedges, _ = axes[row_index, column_index].pie(
                    graph_data["values"],
                )

                axes[row_index, column_index].set_title(graph_data["title"])

                axes[row_index, column_index].legend(
                    wedges,
                    graph_data["labels"],
                    title=graph_data["legend_title"],
                    bbox_to_anchor=(1, 1),
                )

                data_index += 1

        self.logger.info(f"Saving graph {graph_name}")

        result = BytesIO()
        fig.savefig(result, format="png")

        return result

    def create_bar_graph(
        self,
        graph_name,
        data: list[dict],
        title,
        x_label,
        y_label,
        legends: tuple,
    ):
        """Create bar graph from provided data."""
        plot.subplots(figsize=(10, 7))

        plots = []
        bottom = []

        for dataset in data:
            plot_args = {"x": dataset["labels"], "height": dataset["values"]}

            if bottom:
                plot_args["bottom"] = bottom

            plots.append(plot.bar(**plot_args))

            bottom = dataset["values"]

        plot.xlabel(x_label)
        plot.ylabel(y_label)

        plot.title(title)

        plot.legend(tuple([plot[0] for plot in plots]), legends)

        self.logger.info(f"Saving graph {graph_name}")

        result = BytesIO()
        plot.savefig(result, format="png")

        return result
