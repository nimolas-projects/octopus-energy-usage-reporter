"""Handler that deals with API Events."""

import json

from .base_handler import BaseHandler


class APIHandler(BaseHandler):
    """Handler that deals with API Events."""

    def __init__(self):
        """Initialise inheritied classes."""
        super().__init__()

    def parse_event(self):
        """Parse event from SQS event.

        Args:
            - None

        Return:
            - None
        """
        self.body = json.loads(self.event["body"])
