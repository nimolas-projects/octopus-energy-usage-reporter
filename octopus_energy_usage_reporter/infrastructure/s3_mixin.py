"""Mixin for handling S3 operations."""

import boto3


class S3Mixin:
    """Instance to handle S3 operations."""

    s3_client = boto3.client("s3", region_name="eu-west-1")

    def put_object(self, item):
        """Put object to specified bucket."""
        self.logger.info("Putting object", item)

        try:
            self.s3_client.put_object(**item)
        except Exception as e:
            self.logger.error("Error occured when putting object", item, e)
            raise
