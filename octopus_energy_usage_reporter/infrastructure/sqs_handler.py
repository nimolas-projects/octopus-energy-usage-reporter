"""Handler that deal with SQS Events."""

import json

from .base_handler import BaseHandler


class SQSHandler(BaseHandler):
    """Handler that deal with SQS Events."""

    def __init__(self):
        """Initialise inheritied classes."""
        super().__init__()

    def parse_event(self):
        """Parse event from SQS event.

        Args:
            - None

        Return:
            - None
        """
        self.logger.info("Received Event:", self.event)

        self.message_attributes = self.event["Records"][0]["messageAttributes"]

        self.body = json.loads(self.event["Records"][0]["body"])

    def set_correlation_id(self):
        """Set correlation id from SQS event.

        Args:
            - None

        Return:
            - None
        """
        self.correlation_id = self.message_attributes.get("correlation_id", {}).get(
            "stringValue", None
        )
