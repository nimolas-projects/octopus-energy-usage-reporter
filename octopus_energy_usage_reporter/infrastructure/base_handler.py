"""Base Handler. Inherited by other lambdas in the service."""


import json
import uuid

from .logger import Logger


class BaseHandler:
    """Base Handler class."""

    def __init__(self):
        """Create the lambda instance.

        Returns:
            - BaseHandler instance
        """
        self.logger = Logger()
        self.correlation_id = None

    def execute(self):
        """Execute the lambda instance. Must be overwritten.

        Returns:
            - None
        """
        pass

    def parse_event(self):
        """Public function to use values from event. Can be overridden.

        Args:
            - None

        Returns:
            - None
        """
        pass

    def _parse_event(self, event):
        self.event = event

    def parse_context(self):
        """Public function to use values from context. Can be overridden.

        Args:
            - None

        Returns
            - None
        """
        pass

    def _parse_context(self, context):
        self.context = context

    def _parse_event_and_context(self, event, context):
        self._parse_event(event)
        self._parse_context(context)

        self.parse_event()
        self.parse_context()

    def set_correlation_id(self):
        """Public function to set correlation id. Can be overridden.

        Args:
            - None

        Returns
            - None
        """
        pass

    def _set_correlation_id(self):
        self.set_correlation_id()

        if self.correlation_id is None:
            self.correlation_id = str(uuid.uuid4())

        self.logger.set_correlation_id(self.correlation_id)

    def _create_response_body(self, body=None, status_code=200, exception=None):
        response = {}

        response["body"] = body if body else {}
        response["body"] = json.dumps(response["body"])

        if exception:
            response["exception"] = exception

        response["statusCode"] = status_code

        return response

    def handler_function(self, event, context):
        """Lambda entrypoint.

        Args:
            - event: lambda event
            - context: lambda context

        Returns
            - response: dict
        """
        try:
            self._parse_event_and_context(event, context)
            self._set_correlation_id()

            self.logger.info(
                "Starting request",
                {"event": self.event, "context": self.context},
            )

            result = self.execute()

            response = self._create_response_body(body=result)
            self.logger.info("Returning response", response)

            return response
        except Exception as e:
            self.logger.error("Lambda invocation failed", exception=e)
            return self._create_response_body(status_code=500, exception=e)
