"""Logger used throughout service."""

import json
import logging as python_logger
import os
import sys
import traceback

SERVICE_NAME = os.environ.get("SERVICE_NAME")
SOURCE_NAME = os.environ.get("SOURCE_NAME")

logging = python_logger.getLogger(SERVICE_NAME)


class Logger:
    """Logger Class. Uses python logging module."""

    def __init__(self, logging_level=python_logger.INFO):
        """Create the logger instance.

        Args:
            - logging_level: logging.level

        Returns:
            - Logger instance
        """
        logging.setLevel(level=logging_level)

        self.correlation_id = None
        self.redact_keys = ["account", "resources"]
        self._allocated_keys = [
            "service",
            "source",
            "message",
            "data",
            "exception",
        ]

    def _default_parse(self, obj):
        result = None

        try:
            result = str(obj)
        except Exception:
            pass

        return result if result is not None else "Couldn't parse object"

    def _handle_exception(self, exception):
        if isinstance(exception, Exception):
            json_exception = {
                "type": exception.__class__,
                "error": exception.args,
            }

            _, _, exception_traceback = sys.exc_info()

            if exception_traceback is not None:
                json_exception["traceback"] = traceback.format_exc()

            return json_exception

        return exception

    def add_redact_keys(self, keys: list):
        """Add keys to the redact_keys property to be redacted by the logger.

        Args:
            - keys: list of keys

        Returns:
            - None
        """
        if any([key in self._allocated_keys for key in keys]):
            self.warn(
                (
                    "Pre-existing fields detected. "
                    "These will not be added to redact_keys."
                )
            )

        keys = [key for key in keys if key not in self._allocated_keys]

        self.redact_keys.extend(keys)

    def _redact_number(self, data, redact):
        if redact:
            return f"Redacted {len(str(data))} character(s)"

        return data

    def _redact_string(self, data, redact):
        if redact:
            return f"Redacted {len(data)} character(s)"

        return data

    def _redact_logs(self, data, redact=False):
        if isinstance(data, dict):
            return {
                key: self._redact_logs(value, key in self.redact_keys)
                for key, value in data.items()
            }

        if isinstance(data, list):
            return [self._redact_logs(value, redact) for value in data]

        if isinstance(data, int) or isinstance(data, float):
            return self._redact_number(data, redact)

        if isinstance(data, str):
            return self._redact_string(data, redact)

        return data

    def _create_log(self, message, data=None, exception=None):
        log = {
            "service": SERVICE_NAME,
            "source": SOURCE_NAME,
            "message": message,
        }

        if self.correlation_id:
            log["correlation_id"] = self.correlation_id

        if data is not None:
            log["data"] = self._redact_logs(data)

        if exception is not None:
            log["exception"] = self._handle_exception(exception)

        return json.dumps(log, default=self._default_parse, indent=4)

    def set_correlation_id(self, correlation_id):
        """Set correlation ID. Comes from lambda instance.

        Args:
            - correlation_id: string

        Returns:
            - None
        """
        self.correlation_id = correlation_id

    def info(self, message, data=None):
        """Log at info level.

        Args:
            - message: string
            - data: dict

        Returns:
            - None
        """
        logging.info(self._create_log(message, data))

    def warn(self, message, data=None):
        """Log at warn level.

        Args:
            - message: string
            - data: dict

        Returns:
            - None
        """
        logging.warning(self._create_log(message, data))

    def error(self, message, data=None, exception=None):
        """Log at error level.

        Args:
            - message: string
            - data: dict
            - exception: Exception

        Returns:
            - None
        """
        logging.error(self._create_log(message, data, exception))
