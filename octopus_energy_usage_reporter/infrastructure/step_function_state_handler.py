"""Handler with overrides for step function lambdas."""

from .base_handler import BaseHandler


class StepFunctionStateHandler(BaseHandler):
    """Handler that overrides returns for next state."""

    def parse_event(self):
        """
        Parse the event from the previous state.

        Also parses the correlation id from the metadata.
        """
        self.body = self.event["body"]
        self.metadata = self.event["metadata"]

        self.correlation_id = self.metadata.get("correlation_id")

    def _create_response_body(self, body=None, status_code=200, exception=None):
        """Return the body with metadata to the next state."""
        result = {
            "body": None,
            "metadata": {"correlation_id": self.correlation_id},
            "status_code": status_code,
        }

        if body is not None:
            result["body"] = body

        return result
