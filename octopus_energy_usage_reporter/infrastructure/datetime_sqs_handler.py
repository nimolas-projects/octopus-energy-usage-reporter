"""Handler to abstract parsing of DateTime from SQS events."""

from .datetime_mixin import DateTimeMixin
from .sqs_handler import SQSHandler


class DateTimeSQSHandler(SQSHandler, DateTimeMixin):
    """Handler to abstract parsing of DateTime from SQS events."""

    def parse_event(self):
        """Parse the event from the SQS queue.

        Args:
            - None

        Return:
            - None
        """
        super().parse_event()

        self.start_date = self.parse_datetime_string(self.body["start_date"])
        self.end_date = self.parse_datetime_string(self.body["end_date"])
