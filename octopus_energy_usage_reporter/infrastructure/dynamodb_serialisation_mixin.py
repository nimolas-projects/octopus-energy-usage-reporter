"""Helper class that allows deserialisation of DynamoDB responses."""


class DynamoDBSerialisationMixin:
    """Mixin that parses DynamoDB responses."""

    def _unserialise_dynamo_types(self, key, value):
        return (
            str(value)
            if "S" in key
            else float(value)
            if "N" in key and "." in value
            else int(value)
            if "N" in key
            else value
        )

    def parse_dynamodb_response(self, data):
        """Parse DynamoDB response to remove nested dict."""
        return {
            key: self._unserialise_dynamo_types(*value.keys(), *value.values())
            for key, value in data.items()
        }
