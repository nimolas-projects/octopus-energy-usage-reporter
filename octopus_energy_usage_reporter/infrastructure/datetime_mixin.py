"""Module for datetime parsing functionality."""
from datetime import datetime, time, timedelta


class DateTimeMixin:
    """Mixin that deals with shared DateTime functionality."""

    def parse_timestamp(self, timestamp):
        """Convert a epoch timestamp in seconds to datetime object.

        Assumes UTC as timezone.
        """
        return datetime.utcfromtimestamp(timestamp)

    def parse_datetime_string(self, datetime_string):
        """Convert a datetime string to an datetime object.

        Args:
            - datetime_string: str

        Returns:
            - datetime: DateTime
        """
        try:
            return datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%SZ")
        except Exception as e:
            self.logger.error("Incorrect datetime string format", exception=e)
            raise

    def get_weekday_string(self, weekday_int):
        """Return string equivalent for weekday function.

        Args:
            - weekday_int: int

        Return:
            - weekday: str

        """
        days = [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ]

        default = days[0]

        try:
            return days[weekday_int]
        except IndexError:
            self.logger.warn(
                (
                    "Invalid index passed to get_weekday_string.",
                    "Returning default",
                ),
                {"invalid_index": weekday_int},
            )
            return default

    def get_month_string(self, month_int):
        """Return string equivalent for month attribute.

        Args:
            - month_int: int

        Return:
            - month: str

        """
        months = [
            "N/A",
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ]

        default = months[1]

        try:
            return months[month_int]
        except IndexError:
            self.logger.warn(
                (
                    "Invalid index passed to get_month_string.",
                    "Returning default",
                ),
                {"invalid_index": month_int},
            )
            return default

    def subtract_days_from_date(self, datetime_obj, days=1):
        """Return previous date from the one provided.

           Can be more than 1 day, by providing the days param.

        Args:
            - datetime_obj: DateTime
            - days: int

        Return:
            - datetime_obj: Datetime

        """
        return datetime_obj - timedelta(days=days)

    def add_days_to_date(self, datetime_obj, days):
        """Add n days to given date.

        Args:
            - datetime_obj: DateTime
            - days: int

        Return:
            - datetime_obj: Datetime

        """
        return datetime_obj + timedelta(days=days)

    def get_time_ranges(self, date: datetime, start_time, end_time):
        """Return two datetimes between two time ranges.

        Args:
            - date: Datetime
            - start_time: str
            - end_time: str

        Returns:
            - start_date, end_date: Tuple(DateTime)
        """
        start_date = datetime(date.year, date.month, date.day)
        end_date = datetime(date.year, date.month, date.day)

        start_time_object = time.fromisoformat(start_time)
        end_time_object = time.fromisoformat(end_time)

        start_date = datetime.combine(start_date, start_time_object)
        end_date = datetime.combine(end_date, end_time_object)

        return start_date, end_date

    def generate_date_range(self, start_date: datetime, end_date: datetime):
        """Return a generator of range of dates between two dates, inclusive.

        Args:
            - start_date: Datetime
            - end_date: Datetime

        Returns:
            - dates: generator[DateTime]
        """
        delta = end_date - start_date

        return (start_date + timedelta(days=i) for i in range(delta.days + 1))
