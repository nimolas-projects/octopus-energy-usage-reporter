"""Mixin that deals with contacting OctopusEnergy."""

import requests

url = "https://api.octopus.energy/v1"


class OctopusAPIMixin:
    """Mixin that deals with contacting OctopusEnergy."""

    def _make_request(
        self,
        auth,
        request_url,
        method,
        params=None,
        payload=None,
        exclude_codes=None,
    ):
        headers = {"Authorization": f"Token {auth}"}

        try:
            response = requests.request(
                method,
                request_url,
                headers=headers,
                params=params,
                data=payload,
            )

            # I don't like this, this is ugly - Nyk 05/06/22
            if exclude_codes is not None:
                if response.status_code not in exclude_codes:
                    response.raise_for_status()
            else:
                response.raise_for_status()

            return response.json()
        except Exception as e:
            self.logger.error(
                "Failed to make request",
                {"url": request_url, "method": method, "params": params},
                e,
            )
            raise

    def get_product_code(self, auth, product_name, available_at_date):
        """Get the product code for the OctopusEnergy product.

        Args:
            - auth: str
            - product_name: str
            - available_at_date: str

        Return:
            - str
        """
        next_token = ""
        product_code = None

        params = {"available_at": available_at_date}

        while next_token is not None and product_code is None:
            response = self._make_request(auth, url + "/products/", "GET", params=params)
            next_token = response["next"]

            filtered_results = [
                result
                for result in response["results"]
                if result["full_name"] == product_name
            ]

            if filtered_results:
                product_code = filtered_results[0]["code"]

        if product_code is None:
            raise ValueError("Unable to find product code from Octopus API")

        return product_code

    def get_tariff_code(self, auth, product_code, tariff_version, register):
        """Get the tariff code for the OctopusEnergy product.

        Args:
            - auth: str
            - product_code: str
            - tariff_version: str
            - available_at_date: str
            - register: str

        Return:
            - str
        """
        tariff_code = None

        response = self._make_request(
            auth,
            url + f"/products/{product_code}",
            "GET",
        )

        tariff = response.get(register, {})

        filtered_tariff = tariff.get(tariff_version, {})

        tariff_code = filtered_tariff.get("direct_debit_monthly", {}).get("code", None)

        if not tariff_code:
            raise ValueError("Unable to find tariff code from Octopus API")

        return tariff_code

    def _parse_octopus_response(self, response, keys):
        if "results" in response:
            return [
                {key: value for key, value in result.items() if key in keys}
                for result in response["results"]
            ]

        return "N/A"

    def get_tariff_details(
        self,
        auth,
        product_code,
        tariff_code,
        available_from_date,
        available_to_date,
    ):
        """Get the tariff details for the OctopusEnergy product.

        Args:
            - auth: str
            - product_code: str
            - tariff_version: str
            - available_at_date: str
            - register: str

        Return:
            - str
        """
        params = {
            "period_from": available_from_date,
            "period_to": available_to_date,
        }
        api_paths = [
            "standing-charges",
            "standard-unit-rates",
            "day-unit-rates",
            "night-unit-rates",
        ]
        responses = {}

        for path in api_paths:
            response = self._make_request(
                auth,
                url
                + (
                    f"/products/{product_code}"
                    f"/electricity-tariffs/{tariff_code}/{path}"
                ),
                "GET",
                params=params,
                exclude_codes=[400],
            )

            tariff_details = {
                path: self._parse_octopus_response(response, ["value_inc_vat"])
            }
            responses.update(tariff_details)

        return responses

    def get_tariff_prices(
        self,
        auth,
        product_name,
        tariff_version,
        product_date,
        register,
        available_from_date,
        available_to_date,
    ):
        """Get the prices for a given tariff.

        Args:
            - auth: str
            - product_name: str
            - tariff_version: str
            - product_date: str
            - register: str
            - available_from_date: str
            - available_to_date: str

        Return:
            - dict
        """
        self.logger.info(
            "Getting product code",
            {
                "product_name": product_name,
                "tariff_version": tariff_version,
                "product_date": product_date,
                "register": register,
                "available_from": available_from_date,
                "available_to": available_to_date,
            },
        )

        product_code = self.get_product_code(auth, product_name, product_date)

        self.logger.info("Getting tariff code", {"product_code": product_code})
        tariff_code = self.get_tariff_code(auth, product_code, tariff_version, register)

        self.logger.info("Getting tariff details", {"tariff_code": tariff_code})
        return self.get_tariff_details(
            auth,
            product_code,
            tariff_code,
            available_from_date,
            available_to_date,
        )

    def get_meter_consumption(self, auth, mban, serial_number, period_from, period_to):
        """Get the meter consumption for a given meter.

        Args:
            - auth: str
            - mban: str
            - serial_number: str
            - period_from: str
            - period_to: str

        Return:
            - dict
        """
        self.logger.info(
            "Getting meter consumption",
            {
                "mban": mban,
                "serial_number": serial_number,
                "period_from": period_from,
                "period_to": period_to,
            },
        )

        params = {
            "group_by": "day",
            "period_from": period_from,
            "period_to": period_to,
        }

        response = self._make_request(
            auth,
            url
            + (
                f"/electricity-meter-points/{mban}"
                f"/meters/{serial_number}/consumption/"
            ),
            "GET",
            params=params,
        )

        parsed = self._parse_octopus_response(response, ["consumption"])

        return parsed[0]["consumption"]
