"""Class to help make specific stats from Octopus data."""

from statistics import mean, median


class UsageStatsMixin:
    """Mixin to create stats for use in graphs."""

    def _generate_stats(self, collection):
        """Return a dict of stats for a given collection.

        Args:
            - collection: iterable

        Return:
            - dict
        """
        max_value = max(collection)
        min_value = min(collection)
        mean_value = round(mean(collection), 4)
        median_value = round(median(collection), 4)

        return {
            "max": max_value,
            "min": min_value,
            "mean": mean_value,
            "median": median_value,
        }

    def _value_dict_generator(self, time_collection, total_collection):
        return (
            {
                "value": time_collection[index],
                "timestamp": total_collection[index]["DateEpoch"],
            }
            for index in range(len(total_collection))
        )

    def get_stats_for_collection(self, collection):
        """Get all useful stats for a given month of data from DynamoDB.

        Args:
            - collection: iterable

        Return:
            - stats: dict
        """
        day_costings = []
        night_costings = []

        day_usage = []
        night_usage = []

        for item in collection:
            day_price = round(
                (item["DayRate"] * item["DaykWh"] + item["StandingCharge"]) / 100,
                2,
            )
            night_price = round(
                (item["NightRate"] * item["NightkWh"] + item["StandingCharge"]) / 100,
                2,
            )

            day_costings.append(day_price)
            night_costings.append(night_price)

            day_usage.append(item["DaykWh"])
            night_usage.append(item["NightkWh"])

        return {
            "costings": {
                "stats": {
                    "day": self._generate_stats(day_costings),
                    "night": self._generate_stats(night_costings),
                },
                "values": {
                    "day": [
                        value
                        for value in self._value_dict_generator(day_costings, collection)
                    ],
                    "night": [
                        value
                        for value in self._value_dict_generator(
                            night_costings, collection
                        )
                    ],
                },
            },
            "usage": {
                "stats": {
                    "day": self._generate_stats(day_usage),
                    "night": self._generate_stats(night_usage),
                },
                "values": {
                    "day": [
                        value
                        for value in self._value_dict_generator(day_usage, collection)
                    ],
                    "night": [
                        value
                        for value in self._value_dict_generator(night_usage, collection)
                    ],
                },
            },
        }

    def get_stats_for_weekdays(self, collection):
        """Get all useful stats for a given month of data from DynamoDB.

        Filtered by weekday

        Args:
            - collection: iterable

        Return:
            - stats: dict
        """
        return {
            day: self.get_stats_for_collection(data) for day, data in collection.items()
        }
