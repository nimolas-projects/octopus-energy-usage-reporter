
install:
    poetry install

update:
    poetry update

local: install
    poetry run pre-commit install

ruff:
    poetry run ruff check .

ruff-fix:
    poetry run ruff check . --fix-only

cfn-lint:
    poetry run cfn-lint template.yaml --ignore-checks W3005

gitlab-lint:
    @poetry run gll --project 35225064 --token {{env_var('GITLAB_TOKEN')}}

lint: ruff

lint-fix: ruff-fix

test:
    poetry run pytest \
    --cov-report term:skip-covered \
    --cov-report html:coverage \
    --cov-report xml:coverage/coverage.xml \
    --junitxml=coverage/junit.xml \
    --cov-fail-under=95 \
    --cov-branch \
    --cov=octopus_energy_usage_reporter tests/unit_tests -ra -s

test-vv:
    poetry run pytest --cov=octopus_energy_usage_reporter/ tests/ --cov-report html --cov-fail-under=95 --cov-branch --junitxml=coverage/junit.xml -vv

bandit:
    poetry run bandit  --configfile bandit.yaml --recursive octopus_energy_usage_reporter -q

safety:
    poetry run safety check -i 67599

create-requirements:
    poetry export -f requirements.txt -o ./octopus_energy_usage_reporter/requirements.txt --with-credentials --without-hashes

build: create-requirements
    sam build --use-container -m ./octopus_energy_usage_reporter/requirements.txt -t template.yaml

deploy:
    sam deploy \
    --stack-name $CI_PROJECT_NAME--$CI_COMMIT_BRANCH \
    --region eu-west-2 \
    --profile $CI_PROJECT_NAME \
    --resolve-s3 \
    --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM \
    --tags Service=$CI_PROJECT_NAME Repo=$CI_PROJECT_URL Version=$CI_PIPELINE_ID--$CI_JOB_ID \