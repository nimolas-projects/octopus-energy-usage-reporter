import boto3
from core import Logger


class MoveItemsAcrossOrganisationAccounts:
    def __init__(self) -> None:
        self.logger = Logger()

    def get_client(self, session):
        return session.client("ssm", region_name="eu-west-2")

    def get_params(self, client, kwargs):
        return client.get_parameters(**kwargs).get("Parameters")

    def put_param(self, client, kwargs):
        client.put_parameter(**kwargs)

    def execute(self):
        old_account_session = boto3.Session(profile_name="root")
        new_account_session = boto3.Session(profile_name="OEUR")

        old_account_client = self.get_client(old_account_session)
        new_account_client = self.get_client(new_account_session)

        args = {
            "Names": [
                "/OEUR/APIAuth",
                "/OEUR/E-Mail",
                "/OEUR/ElectricityRegister",
                "/OEUR/MBAN",
                "/OEUR/ProductName",
                "/OEUR/ProductStartDate",
                "/OEUR/SerialNumber",
                "/OEUR/TariffVersion",
            ]
        }

        for param in self.get_params(old_account_client, args):
            param_args = {
                "Name": param.get("Name"),
                "Value": param.get("Value"),
                "Type": param.get("Type"),
            }

            self.put_param(new_account_client, param_args)


MoveItemsAcrossOrganisationAccounts().execute()
