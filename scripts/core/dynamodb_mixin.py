class DynamoDBMixin:
    def get_pagianator(self, client, command, params):
        paginator = client.get_paginator(command)
        return paginator.paginate(**params)

    def get_scan(self, client, table_name, select="ALL_ATTRIBUTES"):
        for page in self.get_pagianator(
            client, "scan", {"TableName": table_name, "Select": select}
        ):
            self.logger.info("Got page of items")

            for item in page.get("Items", []):
                self.logger.info("Returning item")

                yield item

    def put_item(self, client, table_name, item):
        self.logger.info("Uploading item", item)

        try:
            client.put_item(TableName=table_name, Item=item)
        except Exception as e:
            self.logger.error("Failed to upload item", item, e)
