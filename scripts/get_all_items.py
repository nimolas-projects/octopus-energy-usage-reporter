import json
import os

from core import DynamoDBMixin, Logger
from dotenv import load_dotenv

load_dotenv()

TABLE_NAME = os.environ["TABLE_NAME"]


class GetAllItems(DynamoDBMixin):
    def __init__(self) -> None:
        self.logger = Logger()

    def get_all_items(self):
        return [item for item in self.get_scan(TABLE_NAME)]

    def execute(self):
        self.logger.info("Creating json file")

        with open("items.json", "w") as file:
            self.logger.info("Getting items from DynamoDB")

            json.dump({"Results": self.get_all_items()}, file)


GetAllItems().execute()
