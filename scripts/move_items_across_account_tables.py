import os

import boto3
from core import DynamoDBMixin, Logger
from dotenv import load_dotenv

load_dotenv()

OLD_TABLE_NAME = os.environ["OLD_TABLE_NAME"]
NEW_TABLE_NAME = os.environ["NEW_TABLE_NAME"]


class MoveItemsAcrossOrganisationAccounts(DynamoDBMixin):
    def __init__(self) -> None:
        self.logger = Logger()

    def make_client_from_session(self, session):
        return session.client("dynamodb", region_name="eu-west-2")

    def execute(self):
        old_account_session = boto3.Session(profile_name="root")
        new_account_session = boto3.Session(profile_name="OEUR")

        old_account_ddb_client = self.make_client_from_session(old_account_session)
        new_account_ddb_client = self.make_client_from_session(new_account_session)

        for item in self.get_scan(old_account_ddb_client, OLD_TABLE_NAME):
            self.put_item(new_account_ddb_client, NEW_TABLE_NAME, item)


MoveItemsAcrossOrganisationAccounts().execute()
